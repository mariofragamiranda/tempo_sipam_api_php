<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$router->get('/api/cidades/{key}', 'CidadesController@getAll');

$router->get('/api/cidadesUf/{key}/{uf}', 'CidadesController@getAllUf');

$router->get('/api/previsao/{key}/{id}', 'PrevisaoController@get');

$router->get('/api/radar/{variavel}/{fonte}', 'RadarController@getImages');


$router->get('/api/pontos/{key}', 'PontosController@getPontos');

$router->get('/api/setPonto/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{velocidade}/{altitude}/{obs}', 'PontosController@setPonto');


$router->get('/api/setPontoImg/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{velocidade}/{altitude}/{obs}/{img}', 'PontosController@setPontoImg');

$router->post('/api/delPonto/', 'PontosController@delPonto');

$router->get('/api/setPontoTeste/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{obs}', 'PontosController@setPontoTeste');

//$router->get('/api/radar/', 'RadarController@getImages');


$router->get('/api/oleo/{key}', 'OleoController@getOleo');

$router->get('/api/setOleo/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{velocidade}/{altitude}/{obs}', 'OleoController@setOleo');


$router->get('/api/setOleoImg/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{velocidade}/{altitude}/{obs}/{telefone}/{p1}/{p2}/{img}', 'OleoController@setOleoImg');

$router->get('/api/setOleoImg2/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{velocidade}/{altitude}/{obs}/{telefone}/{p1}/{p2}/{cpf}/{img}', 'OleoController@setOleoImg2');


$router->post('/api/delOleo/', 'OleoController@delOleo');

$router->get('/api/setOleoTeste/{key}/{data}/{hora}/{latitude}/{longitude}/{acuracia}/{nome_informante}/{email_informante}/{orgao_informante}/{intensidade}/{obs}', 'OleoController@setOleoTeste');

$router->get('/api/testeEnvi/', 'OleoController@testeEnvi');

$router->get('/api/teste2/{key}/', 'OleoController@teste2');

Route::get('/', function () {
    return "ok";//view('welcome');
});

Route::get('/teste/', function () {
    return "ok-teste";//view('welcome');
});