<?php
include '../conexao/Conexao.php';

class Ponto extends Conexao{
    private $id;
	private $dataCaptura;
    private $horaCaptura;
    private $latitude;
    private $longitude;
    private $acuracia;
    private $id_informante;
    private $obs;
    private $tipo;
    private $velocidade;
    private $altitude;
    private $img;

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getDataCaptura(){
		return $this->dataCaptura;
	}

	public function setDataCaptura($dataCaptura){
		$this->dataCaptura = $dataCaptura;
	}

	public function getHoraCaptura(){
		return $this->horaCaptura;
	}

	public function setHoraCaptura($horaCaptura){
		$this->horaCaptura = $horaCaptura;
	}

	public function getLatitude(){
		return $this->latitude;
	}

	public function setLatitude($latitude){
		$this->latitude = $latitude;
	}

	public function getLongitude(){
		return $this->longitude;
	}

	public function setLongitude($longitude){
		$this->longitude = $longitude;
	}

	public function getAcuracia(){
		return $this->acuracia;
	}

	public function setAcuracia($acuracia){
		$this->acuracia = $acuracia;
	}

	public function getId_informante(){
		return $this->id_informante;
	}

	public function setId_informante($id_informante){
		$this->id_informante = $id_informante;
	}

	public function getObs(){
		return $this->obs;
	}

	public function setObs($obs){
		$this->obs = $obs;
	}

	public function getTipo(){
		return $this->tipo;
	}

	public function setTipo($tipo){
		$this->tipo = $tipo;
	}

	public function getVelocidade(){
		return $this->velocidade;
	}

	public function setVelocidade($velocidade){
		$this->velocidade = $velocidade;
	}

	public function getAltitude(){
		return $this->altitude;
	}

	public function setAltitude($altitude){
		$this->altitude = $altitude;
	}

	public function getImg(){
		return $this->img;
	}

	public function setImg($img){
		$this->img = $img;
	}


    public function insert($obj){
    	$sql = "INSERT INTO terascan.ponto(dataCaptura, horaCaptura, latitude, longitude, acuracia, id_informante, obs, tipo, velocidade, altitude, img) VALUES (:dataCaptura,:horaCaptura,:latitude,:longitude,:id_informante,:obs,:tipo,:velocidade,:altitude,:img)";
    	$consulta = Conexao::prepare($sql);
        $consulta->bindValue('dataCaptura',  $obj->dataCaptura);
        $consulta->bindValue('horaCaptura', $obj->horaCaptura);
        $consulta->bindValue('latitude' , $obj->latitude);
        $consulta->bindValue('longitude' , $obj->longitude);
        $consulta->bindValue('id_informante' , $obj->id_informante);
        $consulta->bindValue('obs' , $obj->obs);
        $consulta->bindValue('tipo' , $obj->tipo);
        $consulta->bindValue('velocidade' , $obj->velocidade);
        $consulta->bindValue('altitude' , $obj->altitude);
        $consulta->bindValue('img' , $obj->img);
    	return $consulta->execute();

	}

	public function update($obj,$id = null){
		$sql = "UPDATE terascan.ponto SET dataCaptura = :dataCaptura, horaCaptura = :horaCaptura,latitude = :latitude, longitude = :longitude,id_informante =:id_informante, obs = :obs, tipo = :tipo, velocidade = :velocidade, altitude = :altitude, img = :img   WHERE id = :id ";
		$consulta = Conexao::prepare($sql);
        $consulta->bindValue('dataCaptura',  $obj->dataCaptura);
        $consulta->bindValue('horaCaptura', $obj->horaCaptura);
        $consulta->bindValue('latitude' , $obj->latitude);
        $consulta->bindValue('longitude' , $obj->longitude);
        $consulta->bindValue('id_informante' , $obj->id_informante);
        $consulta->bindValue('obs' , $obj->obs);
        $consulta->bindValue('tipo' , $obj->tipo);
        $consulta->bindValue('velocidade' , $obj->velocidade);
        $consulta->bindValue('altitude' , $obj->altitude);
        $consulta->bindValue('img' , $obj->img);
        $consulta->bindValue('id' , $obj->id);
		return $consulta->execute();
	}

	public function delete($obj,$id = null){
		$sql =  "DELETE FROM terascan.ponto WHERE id = :id";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue('id',$id);
		$consulta->execute();
	}

	public function find($id = null){

	}

	public function findAll(){
		$sql = "SELECT * FROM terascan.ponto";
		$consulta = Conexao::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll();
    }
    

    public function teste(){
		$sql = "SELECT * FROM FROM terascan.alvo_aereo";
		$consulta = Conexao::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll();
	}
}

?>