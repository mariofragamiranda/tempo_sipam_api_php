<?php
include '../../conexao/Conexao.php';

class PontoApp extends Conexao{
    private $id;
	private $dataCaptura;
    private $horaCaptura;
    private $latitude;
    private $longitude;
    private $acuracia;
    private $obs;
    private $intensidade;
    private $velocidade;
    private $altitude;
    private $img;

    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getDataCaptura(){
		return $this->dataCaptura;
	}

	public function setDataCaptura($dataCaptura){
		$this->dataCaptura = $dataCaptura;
	}

	public function getHoraCaptura(){
		return $this->horaCaptura;
	}

	public function setHoraCaptura($horaCaptura){
		$this->horaCaptura = $horaCaptura;
	}

	public function getLatitude(){
		return $this->latitude;
	}

	public function setLatitude($latitude){
		$this->latitude = $latitude;
	}

	public function getLongitude(){
		return $this->longitude;
	}

	public function setLongitude($longitude){
		$this->longitude = $longitude;
	}

	public function getAcuracia(){
		return $this->acuracia;
	}

	public function setAcuracia($acuracia){
		$this->acuracia = $acuracia;
	}

	public function getObs(){
		return $this->obs;
	}

	public function setObs($obs){
		$this->obs = $obs;
	}

	public function getintensidade(){
		return $this->intensidade;
	}

	public function setintensidade($intensidade){
		$this->tipo = $intensidade;
	}

	public function getVelocidade(){
		return $this->velocidade;
	}

	public function setVelocidade($velocidade){
		$this->velocidade = $velocidade;
	}

	public function getAltitude(){
		return $this->altitude;
	}

	public function setAltitude($altitude){
		$this->altitude = $altitude;
	}

	public function getImg(){
		return $this->img;
	}

	public function setImg($img){
		$this->img = $img;
	}

  
	public function find($id = null){

	}

	public function findAll($dataConsulta){
		$sql = "SELECT * FROM terascan.alvo_aereo WHERE data= :dataConsulta and excluido = False";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
		$consulta->execute();
		return $consulta->fetchAll();
    }
    
}

?>