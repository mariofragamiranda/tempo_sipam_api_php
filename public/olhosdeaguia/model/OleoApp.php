<?php
include '../../conexao/Conexao.php';

class OleoApp extends Conexao{
    private $id;
	private $dataCaptura;
    private $horaCaptura;
    private $latitude;
    private $longitude;
    private $acuracia;
    private $obs;
    private $intensidade;
    private $velocidade;
    private $altitude;
	private $img;	
	private $nome_informante;
	private $email_informante;
	private $orgao_informante;
	private $telefone_informante;
	private $p1;
	private $p2;
	private $uf;
	private $municipio;
	private $nome_localidade;
	private $localidade_id;
	private $cpf;


    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getDataCaptura(){
		return $this->dataCaptura;
	}

	public function setDataCaptura($dataCaptura){
		$this->dataCaptura = $dataCaptura;
	}

	public function getHoraCaptura(){
		return $this->horaCaptura;
	}

	public function setHoraCaptura($horaCaptura){
		$this->horaCaptura = $horaCaptura;
	}

	public function getLatitude(){
		return $this->latitude;
	}

	public function setLatitude($latitude){
		$this->latitude = $latitude;
	}

	public function getLongitude(){
		return $this->longitude;
	}

	public function setLongitude($longitude){
		$this->longitude = $longitude;
	}

	public function getAcuracia(){
		return $this->acuracia;
	}

	public function setAcuracia($acuracia){
		$this->acuracia = $acuracia;
	}

	public function getObs(){
		return $this->obs;
	}

	public function setObs($obs){
		$this->obs = $obs;
	}

	public function getintensidade(){
		return $this->intensidade;
	}

	public function setintensidade($intensidade){
		$this->tipo = $intensidade;
	}

	public function getVelocidade(){
		return $this->velocidade;
	}

	public function setVelocidade($velocidade){
		$this->velocidade = $velocidade;
	}

	public function getAltitude(){
		return $this->altitude;
	}

	public function setAltitude($altitude){
		$this->altitude = $altitude;
	}

	public function getImg(){
		return $this->img;
	}

	public function setImg($img){
		$this->img = $img;
	}

	public function getNome_informante(){
		return $this->nome_informante;
	}

	public function setNome_informante($nome_informante){
		$this->nome_informante = $nome_informante;
	}

	public function getEmail_informante(){
		return $this->email_informante;
	}

	public function setEmail_informante($email_informante){
		$this->email_informante = $email_informante;
	}

	public function getOrgao_informante(){
		return $this->orgao_informante;
	}

	public function setOrgao_informante($orgao_informante){
		$this->orgao_informante = $orgao_informante;
	}

	public function getTelefone_informante(){
		return $this->telefone_informante;
	}

	public function setTelefone_informante($telefone_informante){
		$this->telefone_informante = $telefone_informante;
	}

	public function getP1(){
		return $this->p1;
	}

	public function setP1($p1){
		$this->p1 = $p1;
	}

	public function getP2(){
		return $this->p2;
	}

	public function setP2($p2){
		$this->p2 = $p2;
	}


	public function getUf(){
		return $this->uf;
	}

	public function setUf($uf){
		$this->uf = $uf;
	}

	public function getMunicipio(){
		return $this->municipio;
	}

	public function setMunicipio($municipio){
		$this->municipio = $municipio;
	}

	public function getNome_localidade(){
		return $this->nome_localidade;
	}

	public function setNome_localidade($nome_localidade){
		$this->nome_localidade = $nome_localidade;
	}

	public function getLocalidade_id(){
		return $this->localidade_id;
	}

	public function setLocalidade_id($localidade_id){
		$this->localidade_id = $localidade_id;
	}

	public function getCpf(){
		return $this->cpf;
	}

	public function setCpf($cpf){
		$this->cpf = $cpf;
	}

  
	public function find($id = null){

	}

	public function findAll($dataConsulta){
		if($dataConsulta == ""){
			$sql = "SELECT DISTINCT ON (data, latitude, longitude) * FROM terascan.alvo_oleos WHERE excluido = False";
			$consulta = Conexao::prepare($sql);
			$consulta->execute();
			return $consulta->fetchAll();
		} else {
			$sql = "SELECT DISTINCT ON (data, latitude, longitude) * FROM terascan.alvo_oleos WHERE data= :dataConsulta and excluido = False";
			$consulta = Conexao::prepare($sql);
			$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
			$consulta->execute();
			return $consulta->fetchAll();
		}
	}

	public function findAllIntervalo($dataConsulta, $dataConsultaFinal){		
		
			$sql = "SELECT DISTINCT ON (data, latitude, longitude) * FROM terascan.alvo_oleos WHERE data>= :dataConsulta and data<= :dataConsultaFinal and excluido = False";
			$consulta = Conexao::prepare($sql);
			$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
			$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
			$consulta->execute();
			return $consulta->fetchAll();
	}

	public function findId($id){	

		$sql = "SELECT  * FROM terascan.alvo_oleos WHERE id = :id";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue(':id', $id, PDO::PARAM_INT);
		$consulta->execute();
		return $consulta->fetchAll();
}

public function findAvancada($dataConsulta, $dataConsultaFinal, $complemento_sql){
	$sql = "SELECT  * FROM terascan.alvo_oleos WHERE $complemento_sql ( data>= :dataConsulta and data<= :dataConsultaFinal and excluido = False )";
	$consulta = Conexao::prepare($sql);
	$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->execute();
	return $consulta->fetchAll();
}


	public function findRelatorioEstadosOleo($dataConsulta, $dataConsultaFinal, $status){		
		$sql = "SELECT es.estado, oe.uf, count(oe.uf) as total FROM terascan.alvo_oleos as oe INNER JOIN terascan.estados as es ON oe.uf = es.uf WHERE (data>= :dataConsulta and data<= :dataConsultaFinal) and excluido = False and (oe.intensidade =:status) GROUP BY oe.uf,  es.estado";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
		$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
		$consulta->bindValue(':status', $status, PDO::PARAM_INT);
		$consulta->execute();
		return $consulta->fetchAll();
}

public function findRelatorioMunicipioStatus($dataConsulta, $dataConsultaFinal, $status){		
	$sql = "SELECT oe.municipio as municipio , count(oe.municipio) as total FROM terascan.alvo_oleos as oe WHERE (data>= :dataConsulta and data<= :dataConsultaFinal) and excluido = False and (oe.intensidade =:status) GROUP BY oe.municipio ORDER BY total desc;";
	$consulta = Conexao::prepare($sql);
	$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->bindValue(':status', $status, PDO::PARAM_INT);
	$consulta->execute();
	return $consulta->fetchAll();
}

public function findRelatorioStatusPeriodo($dataConsulta, $dataConsultaFinal){		
	$sql = "SELECT data, sum(case when intensidade  = 1 then 1 else 0 end) as inte_1, sum(case when intensidade  = 2 then 1 else 0 end) as inte_2, sum(case when intensidade  = 3 then 1 else 0 end) as inte_3  FROM terascan.alvo_oleos as oe WHERE (data>= :dataConsulta and data<= :dataConsultaFinal) and excluido = False GROUP BY data ORDER BY data;";
	$consulta = Conexao::prepare($sql);
	$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->execute();
	return $consulta->fetchAll();
}

public function findRelatorioStatusUF($dataConsulta, $dataConsultaFinal){		
	$sql = "select ole.uf,  es.estado, sum(case when intensidade  = 1 then 1 else 0 end) as inte_1, sum(case when intensidade  = 2 then 1 else 0 end) as inte_2, sum(case when intensidade  = 3 then 1 else 0 end) as inte_3 ,count(ole.id) as total_geral from terascan.alvo_oleos ole INNER JOIN terascan.estados as es ON es.uf = ole.uf where (ole.uf is not null and ole.excluido = false) and (data>= :dataConsulta and data<= :dataConsultaFinal) GROUP BY ole.uf, es.estado ORDER BY ole.uf;";
	$consulta = Conexao::prepare($sql);
	$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->execute();
	return $consulta->fetchAll();
}

public function findRelatorioStatusGeral($dataConsulta, $dataConsultaFinal){		
	$sql = "select ole.intensidade, count(id) as total from terascan.alvo_oleos ole where (ole.uf is not null and ole.excluido = false) and (data>= :dataConsulta and data<= :dataConsultaFinal) GROUP BY ole.intensidade ORDER BY total desc;";
	$consulta = Conexao::prepare($sql);
	$consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->bindValue(':dataConsultaFinal', $dataConsultaFinal->format('Y-m-d'), PDO::PARAM_STR);
	$consulta->execute();
	return $consulta->fetchAll();
}


public function findEstados(){		
	$sql = "select DISTINCT(ole.UF), estado from terascan.alvo_oleos as ole inner join terascan.estados as es on es.uf = ole.uf  where es.uf is not null  order by uf;	";
	$consulta = Conexao::prepare($sql);
	$consulta->execute();
	return $consulta->fetchAll();
}


public function updateDadosPonto($id, $intensidade, $obs, $excluido){		
	$sql = "UPDATE terascan.alvo_oleos SET excluido = :excluido, intensidade = :intensidade, obs= :obs WHERE id = :id";
	$consulta = Conexao::prepare($sql);
	$consulta->bindValue(':obs', $obs, PDO::PARAM_STR);
	$consulta->bindValue(':excluido', $excluido, PDO::PARAM_BOOL);
	$consulta->bindValue(':intensidade', $intensidade, PDO::PARAM_INT);
	$consulta->bindValue('id', $id);
	return $consulta->execute();	
}


//


}

?>