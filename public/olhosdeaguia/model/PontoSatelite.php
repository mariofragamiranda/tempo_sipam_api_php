<?php
include '../../conexao/Conexao.php';

class PontoSatelite extends Conexao2{

private $uf;
private $data;
private $hora;
private $satelite;
private $potencia;
private $pixel;
private $latitude;
private $longitude;

public function getUf(){
    return $this->uf;
}

public function setUf($uf){
    $this->uf = $uf;
}

public function getData(){
    return $this->data;
}

public function setData($data){
    $this->data = $data;
}

public function getHora(){
    return $this->hora;
}

public function setHora($hora){
    $this->hora = $hora;
}

public function getSatelite(){
    return $this->satelite;
}

public function setSatelite($satelite){
    $this->satelite = $satelite;
}

public function getPotencia(){
    return $this->potencia;
}

public function setPotencia($potencia){
    $this->potencia = $potencia;
}

public function getPixel(){
    return $this->pixel;
}

public function setPixel($pixel){
    $this->pixel = $pixel;
}

public function getLatitude(){
    return $this->latitude;
}

public function setLatitude($latitude){
    $this->latitude = $latitude;
}

public function getLongitude(){
    return $this->longitude;
}

public function setLongitude($longitude){
    $this->longitude = $longitude;
}


public function findAll($uf, $satelite, $dataConsulta){
    $sql = "select uf.sigla as uf, fc.data as data, fc.hora as hora, fc.sat as satelite, fc.potencia_media as potencia, fc.latitude as latitude, fc.longitude as longitude, pixel_count as pixel from terascan.sipam_foco_calor fc inner join public.tb_area_atuacao_cr uf on st_within(fc.the_geom, uf.geom) where fc.data= :dataConsulta and fc.sat= :satelite and uf.sigla= :uf and fc.potencia_media >=15;";
   // echo $sql;
    $consulta = Conexao2::prepare($sql);
    $consulta->bindValue(':uf', $uf, PDO::PARAM_STR);
    $consulta->bindValue(':satelite', $satelite, PDO::PARAM_STR);
    $consulta->bindValue(':dataConsulta', $dataConsulta->format('Y-m-d'), PDO::PARAM_STR);
    $consulta->execute();
    return $consulta->fetchAll();


}


public function findTi($dataConsultaInicio, $dataConsultaFim, $idTi){    

    $sql = " select fc.data as data, fc.hora as hora, fc.sat as satelite, fc.potencia_media as potencia, fc.latitude as latitude, fc.longitude as longitude, pixel_count as pixel from terascan.sipam_foco_calor fc inner join terascan.limites_ti_ro ti on st_within(ST_Transform(fc.the_geom, 4326), ST_Transform(ti.geom, 4326))  where (fc.data >= :dataConsultaInicio and  fc.data <= :dataConsultaFim) and ti.gid= :idTi and fc.sat= 'npp' and fc.potencia_media >=1  LIMIT 250;";
    $consulta = Conexao::prepare($sql);
    $consulta->bindValue(':dataConsultaInicio', $dataConsultaInicio->format('Y-m-d'), PDO::PARAM_STR);
    $consulta->bindValue(':dataConsultaFim', $dataConsultaFim->format('Y-m-d'), PDO::PARAM_STR);
    $consulta->bindValue(':idTi', $idTi, PDO::PARAM_INT);
    $consulta->execute();
    return $consulta->fetchAll();
}


}

?>