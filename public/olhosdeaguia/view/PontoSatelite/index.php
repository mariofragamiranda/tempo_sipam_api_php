<?php
include '../../control/PontoSateliteControl.php';
$PontoSateliteControl = new PontoSateliteControl();
//header('Content-Type: application/json');


$dataConsulta =DateTime::createFromFormat('d/m/Y', $_GET['dataConsulta']);
$uf = $_GET['uf'];
$satelite = $_GET['satelite'];

$coordenadasCentrais = "-4.919726, -59.009217";



switch ($uf) {
    case "RO":
		$coordenadasCentrais =  "-10.789926, -62.816641";
        break;
     case "AC":
		$coordenadasCentrais =  "-8.936160, -70.403344";
		break;
	case "MT":
		$coordenadasCentrais =  "-11.947869, -56.012024";
        break;
	case "AM":
		$coordenadasCentrais =  "-4.225696, -64.190472";
		break;
	case "RR":
		$coordenadasCentrais =  "2.005844, -61.405736";
		break;
	case "AP":
		$coordenadasCentrais =  "1.448750, -51.953739";
		break;
	case "PA":
		$coordenadasCentrais =  "-4.207519, -51.966547";
		break;
	case "TO":
		$coordenadasCentrais =  "-10.291387, -48.201028";
		break;
	case "MA":
		$coordenadasCentrais =  "-4.026802, -45.571097";
		break;	
		

	}


?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Olhos de Águia</title>
</head>
<body>

<style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    #map {
        height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
    }

        #legend h3 {
            margin-top: 0;
        }

        #legend img {
            vertical-align: middle;
            image-orientation: from-image;
        }

</style>


<div id="map"></div>

<script>

    function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5.5,
            mapTypeId: 'OSM',
            center: new google.maps.LatLng(<?php echo $coordenadasCentrais; ?>),
            mapTypeControl: true,
            streetViewControl: false          
        });

        var infoWin = new google.maps.InfoWindow();

        var markers = locations.map(function (location, i) {

            var marker = new google.maps.Marker({
                position: location,
                icon: {
                    url: '../img/fogo.png',
                    size: new google.maps.Size(30, 30),
                    scaledSize: new google.maps.Size(30, 30),
                    anchor: new google.maps.Point(10, 30)
                }
                });

            google.maps.event.addListener(marker, 'click', function (evt) {
                                infoWin.setContent(location.info);
                                infoWin.open(map, marker);
                            })
            return marker;
                            });
       

        //Define OSM map type pointing at the OpenStreetMap tile server
        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                                // "Wrap" x (logitude) at 180th meridian properly
                                // NB: Don't touch coord.x because coord param is by reference, and changing its x property breakes something in Google's lib
                                var tilesPerGlobe = 1 << zoom;
                                var x = coord.x % tilesPerGlobe;
                                if (x < 0) {
                                    x = tilesPerGlobe + x;
                                }
                                // Wrap y (latitude) in a like manner if you want to enable vertical infinite scroll

                                return "http://tile.openstreetmap.org/" + zoom + "/" + x + "/" + coord.y + ".png";
                            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            minimumClusterSize: 4,            
            maxZoom: 19
        }));


        var clusterStyles = [
            {
        url: '../img/m1.png',
        height: 50,
        width: 50,
        anchor: [16, 0],
        textColor: 'transparent',
        textSize: 10
      }, {
        url: '../img/m2.png',
        height: 60,
        width: 60,
        anchor: [24, 0],
        textColor: 'transparent',
        textSize: 0
      }, {
        url: '../img/m3.png',
        height: 66,
        width: 66,
        anchor: [32, 0],
        textColor: 'transparent',
        textSize: 12
      }
        ];

      mcOptions = {
          
        }




                        // Add a marker clusterer to manage the markers.
                        //var markerCluster = new MarkerClusterer(map, markers, mcOptions);
                         var markerCluster = new MarkerClusterer(map, markers,
            { styles: clusterStyles, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }


                        /**/
                        var locations = [
							<?php
							foreach($PontoSateliteControl->findAll($uf, $satelite, $dataConsulta) as $valor){ 

  							echo "{ lat: $valor->latitude , lng: $valor->longitude , info: \"<b>Data: </b> $valor->data <br> <b>Coordenadas: </b> $valor->latitude, $valor->longitude <br> <b> Pixel Count: </b> $valor->pixel    \" }, ";


						}
						?>

    ];

                        //google.maps.event.addDomListener(window, "load", initMap);



</script>


<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">

</script>



<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARsZAdKJtlMX_6g83BzAMkY4CGPtbu05k&libraries=visualization&callback=initMap">
</script>


</body>
</html>


