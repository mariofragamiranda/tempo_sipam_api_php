<?php
header('Content-Type: text/html; charset=utf-8');
include '../../control/OleoAppControl.php';
include '../../csv/csv.php';

$OleoAppControl = new OleoAppControl();

session_start();

if($_SESSION["logado"] != "logado"){
    header('Location: '.'login.php');
}


$dataCampoBusca = "";
$dataCampoBuscaFinal = "";
$linkCSV = "csv.php";

if(empty($_GET['dataConsulta'])) {
  
  $dataCampoBusca = date('d/m/Y',strtotime("-2 day"));
  $dataCampoBuscaFinal = date('d/m/Y');

 $dataConsulta =DateTime::createFromFormat('d/m/Y', date('d/m/Y', strtotime("-2 day")));
 $dataConsultaFinal =DateTime::createFromFormat('d/m/Y', date('d/m/Y'));


} else {
 $dataConsulta =DateTime::createFromFormat('d/m/Y', $_GET['dataConsulta']);
 $dataConsultaFinal =DateTime::createFromFormat('d/m/Y', $_GET['dataConsultaFinal']);

 //exit();

 $dataCampoBusca = $dataConsulta->format('d/m/Y');
 $dataCampoBuscaFinal = $dataConsultaFinal->format('d/m/Y');

 $linkCSV = "csv.php?dataConsulta=".$_GET['dataConsulta'];
}

$coordenadasCentrais = "-10.071667, -35.271126";
  $dados = $OleoAppControl->findAllIntervalo($dataConsulta, $dataConsultaFinal);

download_send_headers("data_export_" . date("Y-m-d") . ".csv");
echo array2csv($dados);
die();
?>