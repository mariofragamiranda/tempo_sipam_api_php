<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="<?php echo($links['index']); ?>">
                    <span data-feather="map"></span>
                    Página Inicial (Mapa) <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['pesquisaAvancada']); ?>">
                    <span data-feather="search"></span>
                    Pesquisa Avançada <span class="sr-only"></span>
                </a>
            </li>
        </ul>

        <?php if($isAdm) { ?>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>ADMINISTRAÇÃO</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="layers"></span>
            </a>
        </h6>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="adm.php">
                    <span data-feather="edit"></span>
                   Correção de Ponto
                </a>
            </li>
        </ul>
       

        <?php } ?>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>RELATÓRIOS</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="layers"></span>
            </a>
        </h6>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['relatoriosEstados']); ?>">
                    <span data-feather="pie-chart"></span>
                    Pontos por Estados
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['relatoriosMunicipios']); ?>">
                    <span data-feather="bar-chart"></span>
                    Pontos por Municípios
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['relatorioStatusPeriodo']); ?>">
                    <span data-feather="pie-chart"></span>
                    Status por Período
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['relatorioStatusEstado']); ?>">
                    <span data-feather="bar-chart"></span>
                    Status por Estado
                </a>
            </li>
                
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['relatorioSituacaoOcorrencia']); ?>">
                    <span data-feather="pie-chart"></span>
                    Situação das Ocorrências
                </a>
            </li>

        </ul>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span> Downloads </span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="layers"></span>
            </a>
        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['csv']); ?>">
                    <span data-feather="download-cloud"></span>
                    Baixar Arquivo CSV
                </a>
            </li>
        </ul>


        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span> DOCUMENTAÇÃO </span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="layers"></span>
            </a>

        </h6>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo($links['manual']); ?>">
                    <span data-feather="help-circle"></span>
                    Manual de Orientações
                </a>
            </li>

        </ul>
        


        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span></span>

        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="logout.php">
                    <span data-feather="log-out"></span>
                    Sair
                </a>
            </li>

        </ul>





    </div>
</nav>