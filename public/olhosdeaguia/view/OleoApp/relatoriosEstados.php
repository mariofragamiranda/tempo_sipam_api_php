<?php
include 'pre_topo.php';

$listaPontos1 = $OleoAppControl->findRelatorioEstadosOleo($dataConsulta, $dataConsultaFinal, 1);
$listaPontos2 = $OleoAppControl->findRelatorioEstadosOleo($dataConsulta, $dataConsultaFinal, 2);
$listaPontos3 = $OleoAppControl->findRelatorioEstadosOleo($dataConsulta, $dataConsultaFinal, 3);




?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="sticky-footer-navbar.css" rel="stylesheet">
    <link href="offcanvas.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart1);
      function drawChart1() {
        var data = google.visualization.arrayToDataTable([
          ['Manchas de Óleo', 'Por Estado'],
          <?php
          	foreach($listaPontos1 as $item){ echo("['$item->estado -  $item->total', $item->total],"); }
          ?>
        ]);

        var options = {
          title: 'Oleada - Manchas - Incidência de Pontos  por Estado:',
        
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_1'));
        chart.draw(data, options);
      }


      google.charts.setOnLoadCallback(drawChart2);
      function drawChart2() {
        var data = google.visualization.arrayToDataTable([
          ['Manchas de Óleo', 'Por Estado'],
          <?php
          	foreach($listaPontos2 as $item){ echo("['$item->estado -  $item->total', $item->total],"); }
          ?>
        ]);

        var options = {
          title: 'Oleada - Vestígios / Esparsos - Incidência de Pontos  por Estado:',        
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_2'));
        chart.draw(data, options);
      }

      google.charts.setOnLoadCallback(drawChart3);
      function drawChart3() {
        var data = google.visualization.arrayToDataTable([
          ['Manchas de Óleo', 'Por Estado'],
          <?php
          	foreach($listaPontos3 as $item){ echo("['$item->estado -  $item->total', $item->total],"); }
          ?>
        ]);

        var options = {
          title: 'Óleo Não Observado / Esparsos - Incidência de Pontos  por Estado:',        
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_3'));
        chart.draw(data, options);
      }


    </script>


</head>

<body class="d-flex flex-column h-100 bg-light">

    <header class="fixed-top">
        <?php include 'topo.php'; ?>
    </header>


    <div class="container-fluid" id="container-olho">
        <div class="row">

        <?php include 'menu.php'; ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

                <h2 class="mt-4"><?php echo("Relatório por Estado - $dataCampoBusca até $dataCampoBuscaFinal") ?></h2>
                <br>

                <div id="piechart_3d_1" style="width: 100%; height: 400px;"></div>

                <div id="piechart_3d_2" style="width: 100%; height: 400px;"></div>

                <div id="piechart_3d_3" style="width: 100%; height: 400px;"></div>
            </main>

        </div>
    </div>





    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

</body>

</html>