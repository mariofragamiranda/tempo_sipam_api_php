<?php
include 'pre_topo.php';

?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="sticky-footer-navbar.css" rel="stylesheet">
    <link href="offcanvas.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>
  </head>
  <body class="d-flex flex-column h-100">
 

  <header class="fixed-top">
  <?php include 'topo.php'; ?>
  <?php include 'topo_secundario.php'; ?>
 </header>
    
    <!-- Começa o conteúdo da página -->
    <main role="main" id="map">       

    <script>

      // This example requires the Visualization library. Include the libraries=visualization
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=visualization">

      var map, heatmap;

      function initMap() {


        var mapTypeIds = [];
            for(var type in google.maps.MapTypeId) {
                mapTypeIds.push(google.maps.MapTypeId[type]);
            }
            mapTypeIds.push("OSM");

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            mapTypeId: 'OSM',
            center: new google.maps.LatLng(<?php echo $coordenadasCentrais; ?>),
            mapTypeControl: true, 
            
            
            mapTypeControlOptions: {
                    mapTypeIds: mapTypeIds,                    
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
            streetViewControl: false,    
        });


        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                                // "Wrap" x (logitude) at 180th meridian properly
                                // NB: Don't touch coord.x because coord param is by reference, and changing its x property breakes something in Google's lib
                                var tilesPerGlobe = 1 << zoom;
                                var x = coord.x % tilesPerGlobe;
                                if (x < 0) {
                                    x = tilesPerGlobe + x;
                                }
                                // Wrap y (latitude) in a like manner if you want to enable vertical infinite scroll

                                return "http://tile.openstreetmap.org/" + zoom + "/" + x + "/" + coord.y + ".png";
                            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));



        heatmap = new google.maps.visualization.HeatmapLayer({
          data: getPoints(),
          radius: 40,          
          map: map
        });
      }
      /*
      gradient: [
          'rgba(0, 0, 0, 0.7)',
          'rgba(0, 0, 0, 0.8)',
          'rgba(0, 0, 0, 0.9)',
          'rgba(0, 0, 0, 1)'
        ],
      */


      

      // Heatmap data: 500 Points
      function getPoints() {
        return [
            <?php foreach($listaPontos as $valor){ echo("new google.maps.LatLng($valor->latitude,  $valor->longitude),");  } ?>
        ];
      }
    </script>
    

 <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script> 
<!--<script src="http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/view/markerclusterer.js"></script> -->


<?php if($_SERVER['HTTP_HOST'] == "http://appportalmeteorologia.sipam.gov.br"){   ?>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpmpfInYyVoBSD8nXI5OuUhmCimAs-7ck&libraries=visualization&callback=initMap"></script>
<?php  } else { ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARsZAdKJtlMX_6g83BzAMkY4CGPtbu05k&libraries=visualization&callback=initMap"></script>
<?php } ?>

    </main>

    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-muted">&copy; Sistema de Proteção da Amazônia - Olhos de Águia - <b> Manchas de Óleo </b> - 2019</span>
      </div>
    </footer>

    

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <?php
/*
var_dump($dataConsulta);
var_dump($dataConsultaFinal);
*/
?>

  </body>
</html>
