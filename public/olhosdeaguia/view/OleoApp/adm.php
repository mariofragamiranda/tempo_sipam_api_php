<?php
include 'pre_topo.php';



if($_SESSION["adm"] != "adm"){
    header('Location: '.'login.php');
}


$mostrarMsg = false;
$textoMsg = "";


if(isset($_POST['gridRadios'])){

    $intensidade =$_POST['gridRadios'];
   // $obs =$_POST['obs'];

    $whatObs = array('\n','\r\n','\r','"',"'", '\r','/\s/',chr(10), '<br />', chr(13));
    $byObs   = array('','', ' ',' ',' ',' ',' ', '', ' ', ' ');  

    $obs = nl2br($_POST['obs']);

    $obs = str_replace($whatObs, $byObs, $obs);  

    $excluido =$_POST['excluido'];
    $idRegistro = $_POST['idRegistro'];   

    $retorno = $OleoAppControl->updateDadosPonto($idRegistro, $intensidade, $obs, $excluido);

    $mostrarMsg = true;
    $textoMsg = "Dados Alterado com Sucesso!";

    $listaPontos = $OleoAppControl->findId($_GET['id']);
}

?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="sticky-footer-navbar.css" rel="stylesheet">
    <link href="offcanvas.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>

</head>

<body class="d-flex flex-column h-100 bg-light">

    <header class="fixed-top">
        <?php include 'topo.php'; ?>
    </header>


    <div class="container-fluid" id="container-olho">
        <div class="row">

            <?php include 'menu.php'; ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

            <?php  if($mostrarMsg){ ?>

            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                <strong><?php echo($textoMsg); ?></strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php } ?>


                <?php  if(!isset($_GET['buscaAvancada'])){ ?>

                <h2 class="mt-4"><?php echo("Correção de Ponto") ?></h2> <br>

                <form class="form-inline" method="get">
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="hidden" name="buscaAvancada" value="1" />
                        <input type="text" class="form-control" id="id" name="id" placeholder="ID" required>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Carregar Ponto</button>
                </form>
                <?php } ?>

                <?php  if(isset($_GET['buscaAvancada']) && $_GET['buscaAvancada'] == 1){ ?>

                <h2 class="mt-4"><?php echo("Formulário de Alteração") ?></h2> <br>

                <form  method="post">

                    <div class="form-row">
                    <div class="form-group col-md-2">

                    <input type="hidden" name="idRegistro" value="<?php echo($listaPontos[0]->id); ?>" />
                        <b> <label for="inputEmail4">Data / Hora</label> </b> <br>                            
                            <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->data); ?>" >
                        </div>
                        <div class="form-group col-md-2">
                        <b> <label for="inputEmail4">Latitude</label>  </b> <br>
                        <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->latitude); ?>" >
                        </div>
                        <div class="form-group col-md-2">
                        <b>  <label for="inputPassword4">Longitude</label>  </b> <br>
                        <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->longitude); ?>" >
                        </div>
                    </div>

                    <div class="form-row">
                    <div class="form-group col-md-2">
                        <b> <label for="inputEmail4">Informante</label> </b> <br>                            
                            <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->nome_informante); ?>" >
                        </div>
                        <div class="form-group col-md-2">
                        <b> <label for="inputEmail4">Orgão</label>  </b> <br>
                        <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->orgao_informante); ?>" >
                        </div>
                        <div class="form-group col-md-2">
                        <b>  <label for="inputPassword4">Celular</label>  </b> <br>
                        <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->telefone_informante); ?>" >
                        </div>
                    </div>

                    <div class="form-row">
                    <div class="form-group col-md-2">
                        <b> <label for="inputEmail4">Localidade</label> </b> <br>                            
                            <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->nome_localidade); ?>" >
                        </div>
                        <div class="form-group col-md-2">
                        <b> <label for="inputEmail4">Município</label>  </b> <br>
                        <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->municipio); ?>" >
                        </div>
                        <div class="form-group col-md-2">
                        <b>  <label for="inputPassword4">UF</label>  </b> <br>
                        <input type="email" class="form-control" id="inputEmail4" disabled value="<?php echo($listaPontos[0]->uf); ?>" >
                        </div>
                    </div>


                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Situação do Local</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1"
                                        value="1" <?php if($listaPontos[0]->intensidade == 1) { echo("checked"); } ?>>
                                    <label class="form-check-label" for="gridRadios1">
                                        Oleada - Manchas
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2"
                                        value="2" <?php if($listaPontos[0]->intensidade == 2) { echo("checked"); } ?>>
                                    <label class="form-check-label" for="gridRadios2">
                                        Oleada - Vestigios / Esparsos
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2"
                                        value="3" <?php if($listaPontos[0]->intensidade == 3) { echo("checked"); } ?>>
                                    <label class="form-check-label" for="gridRadios2">
                                        Óleo Nao Observado
                                    </label>
                                </div>

                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group col-md-4">
                        <label for="exampleFormControlTextarea1">Observação</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="obs"
                            rows="4"><?php echo($listaPontos[0]->obs); ?></textarea>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="excluido">Ponto Excluído?</label>
                        <select class="form-control" id="excluido" name="excluido">
                            <option value="false" <?php if($listaPontos[0]->excluido == false) { echo("selected"); } ?>>Não</option>
                            <option value="true" <?php if($listaPontos[0]->excluido == true) { echo("selected"); } ?>>Sim</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">

                        <button type="submit" class="btn btn-primary">Salvar Alterações</button>
                    </div>

                </form>

                <?php } ?>

            </main>

        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

</body>

</html>