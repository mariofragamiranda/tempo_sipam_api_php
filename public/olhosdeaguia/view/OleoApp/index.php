<?php
include 'pre_topo.php';


 

?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="sticky-footer-navbar.css" rel="stylesheet">
    <link href="offcanvas.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>
  </head>
  <body class="d-flex flex-column h-100">
 

  <header class="fixed-top">
  <?php include 'topo.php'; ?>
  <?php include 'topo_secundario.php'; ?>
 </header>
    
    <!-- Começa o conteúdo da página -->
    <main role="main" id="map">       

   <script>

    function initMap() {


          var mapTypeIds = [];
            for(var type in google.maps.MapTypeId) {
                mapTypeIds.push(google.maps.MapTypeId[type]);
            }
            mapTypeIds.push("OSM");

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            mapTypeId: 'OSM',
            center: new google.maps.LatLng(<?php echo $coordenadasCentrais; ?>),
            mapTypeControl: true,          
            mapTypeControlOptions: {
                    mapTypeIds: mapTypeIds,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
            streetViewControl: false,    
        });

        var infoWin = new google.maps.InfoWindow();

        var markers = locations.map(function (location, i) {

            var marker = new google.maps.Marker({
                position: location,       
                icon: {
                    url: 'http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/img/'+location.icone+'.png',
                    size: new google.maps.Size(32, 32),
                    scaledSize: new google.maps.Size(30, 30),
                    anchor: new google.maps.Point(10, 30)
                }
                });

            google.maps.event.addListener(marker, 'click', function (evt) {
                                infoWin.setContent(location.info);
                                infoWin.open(map, marker);
                            })
                          return marker;
                            });
       

        //Define OSM map type pointing at the OpenStreetMap tile server
        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                                // "Wrap" x (logitude) at 180th meridian properly
                                // NB: Don't touch coord.x because coord param is by reference, and changing its x property breakes something in Google's lib
                                var tilesPerGlobe = 1 << zoom;
                                var x = coord.x % tilesPerGlobe;
                                if (x < 0) {
                                    x = tilesPerGlobe + x;
                                }
                                // Wrap y (latitude) in a like manner if you want to enable vertical infinite scroll

                                return "http://tile.openstreetmap.org/" + zoom + "/" + x + "/" + coord.y + ".png";
                            },
                                  tileSize: new google.maps.Size(256, 256),
                                  name: "OpenStreetMap",
                                  maxZoom: 18
                              }));
      
                        // Add a marker clusterer to manage the markers.
                        //var markerCluster = new MarkerClusterer(map, markers, mcOptions);
                         var markerCluster = new MarkerClusterer(map, markers,
            { gridSize: 50, maxZoom: 17, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' }); }
                        /**/
                        var locations = [
							<?php

							foreach($listaPontos as $valor){                                
                                $dataformatada = date_format(date_create($valor->data), 'd/m/Y');
                                $intensi = getInte($valor->intensidade);
                                $p1c= convB($valor->p1);
                                $p2c= convB($valor->p2);
                               
                                $what = array('\n','\r\n','\r','"',"'", 'www.', '\r','/\s/',chr(10), chr(13));
                                $by   = array(' ',' ', ' ',' ',' ','files.',' ',' ', ' ', ' ' );                            
                                $municpio_tratado = str_replace($what, $by, $valor->municipio);                              
                                $localidade = str_replace($what, $by, $valor->nome_localidade); 
                                $obsTratada = htmlentities($valor->obs, null, 'utf-8');                                
                                
                                $informante_tratado =  str_replace($what, $by, $valor->nome_informante. ' - '.$valor->orgao_informante);

                               // $informante_tratado =  $informante_tratado. ' - '.$valor->orgao_informante); 

                                $obsTratada = str_replace($what, $by, $obsTratada);  
                                
                               // $obsTratada= $valor->obs;//htmlspecialchars($obsTratada);


                              if($valor->nome_localidade != null){
                                $localidade_tratada = "<br> $localidade - $valor->municipio - $valor->uf";
                              } else {
                                $localidade_tratada = "<br> $valor->municipio - $valor->uf";
                              }
                              
                              if(strpos($valor->img, '|') != ""){
                                  $n = strpos($valor->img, ' |');
                                  $urlTratado = substr($valor->img,0,$n);
                              } else {
                                $urlTratado = $valor->img;
                              }
                              
                              $urlTratado = str_replace($what, $by, $urlTratado);  

                             // exit();
                             

                           //   $urlTratado = substr($valor->img, 0, );


  							echo "{ lat: $valor->latitude, lng: $valor->longitude, icone:$valor->intensidade, info: \"<b> ID: </b> $valor->id - $dataformatada - $valor->hora <br> <b>  $intensi </b> $localidade_tratada  <br> <b>Coordenadas: </b> $valor->latitude, $valor->longitude  <br> <b> Equipe no local?</b> $p2c <br> <b> Limpeza concluída?:</b> $p1c <br>  <b> Informante: </b> $informante_tratado  <br> <b> Obs:</b> $obsTratada   <br> <a href='$urlTratado' target='_blank'><img src='$urlTratado' width='300px'  /> </a>   \" }, ";
                echo "\n \n";    
              }
						?>

    ];

                        //google.maps.event.addDomListener(window, "load", initMap);
</script>

 <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script> 
<!--<script src="http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/view/markerclusterer.js"></script> -->


<?php if($_SERVER['HTTP_HOST'] == "http://appportalmeteorologia.sipam.gov.br"){   ?>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpmpfInYyVoBSD8nXI5OuUhmCimAs-7ck&libraries=visualization&callback=initMap"></script>
<?php  } else { ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARsZAdKJtlMX_6g83BzAMkY4CGPtbu05k&libraries=visualization&callback=initMap"></script>
<?php } ?>

    </main>

    <footer class="footer mt-auto py-3">
      <div class="container">
        <span class="text-muted">&copy; Sistema de Proteção da Amazônia - Olhos de Águia - <b> Manchas de Óleo </b> - 2019</span>
      </div>
    </footer>

    

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <?php
/*
var_dump($dataConsulta);
var_dump($dataConsultaFinal);
*/
?>

  </body>
</html>
