<?php
include '../../control/OleoAppControl.php';
$OleoAppControl = new OleoAppControl();

$dataConsulta =DateTime::createFromFormat('d/m/Y', $_GET['dataConsulta']);


$coordenadasCentrais = "-8.071667, -37.271126";

?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Olhos de Águia - Manchas de Oleo</title>
</head>
<body>

<style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    #map {
        height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
    }

        #legend h3 {
            margin-top: 0;
        }

        #legend img {
            vertical-align: middle;
            image-orientation: from-image;
        }

</style>


<div id="map"></div>

<script>

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5.4,
            mapTypeId: 'OSM',
            center: new google.maps.LatLng(<?php echo $coordenadasCentrais; ?>),
            mapTypeControl: true,
            streetViewControl: false          
        });

        var infoWin = new google.maps.InfoWindow();

        var markers = locations.map(function (location, i) {

            var marker = new google.maps.Marker({
                position: location,
                icon: {
                    url: 'http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/img/'+location.icone+'.png',
                    size: new google.maps.Size(30, 30),
                    scaledSize: new google.maps.Size(30, 30),
                    anchor: new google.maps.Point(10, 30)
                }
                });

            google.maps.event.addListener(marker, 'click', function (evt) {
                                infoWin.setContent(location.info);
                                infoWin.open(map, marker);
                            })
            return marker;
                            });
       

        //Define OSM map type pointing at the OpenStreetMap tile server
        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                                // "Wrap" x (logitude) at 180th meridian properly
                                // NB: Don't touch coord.x because coord param is by reference, and changing its x property breakes something in Google's lib
                                var tilesPerGlobe = 1 << zoom;
                                var x = coord.x % tilesPerGlobe;
                                if (x < 0) {
                                    x = tilesPerGlobe + x;
                                }
                                // Wrap y (latitude) in a like manner if you want to enable vertical infinite scroll

                                return "http://tile.openstreetmap.org/" + zoom + "/" + x + "/" + coord.y + ".png";
                            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            minimumClusterSize: 4,            
            maxZoom: 19
        }));


        var clusterStyles = [
            {
        url: 'http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/img/m1.png',
        height: 50,
        width: 50,
        anchor: [16, 0],
        textColor: 'transparent',
        textSize: 10
      }, {
        url: 'http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/img/m2.png',
        height: 60,
        width: 60,
        anchor: [24, 0],
        textColor: 'transparent',
        textSize: 0
      }, {
        url: 'http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/img/m3.png',
        height: 66,
        width: 66,
        anchor: [32, 0],
        textColor: 'transparent',
        textSize: 12
      }
        ];

      mcOptions = {
          
        }
      
                        // Add a marker clusterer to manage the markers.
                        //var markerCluster = new MarkerClusterer(map, markers, mcOptions);
                         var markerCluster = new MarkerClusterer(map, markers,
            { gridSize: 50, maxZoom: 17, styles: clusterStyles, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }


                        /**/
                        var locations = [
							<?php
                                    function getInte($inte)
                                    {
                                        if ($inte == 1) {
                                            return "Oleada - Manchas";
                                        }
                                        if ($inte == 2) {
                                            return "Oleada - Vestígios / Esparsos";
                                        }
                                        if ($inte == 3) {
                                            return "Óleo Não Observado";
                                        }
                                    }

                                    Function convB($b){
                                        if($b){
                                            return "Sim";
                                        } else {
                                            return "Não";
                                        }
                                    }

							foreach($OleoAppControl->findAll($dataConsulta) as $valor){                                
                                $dataformatada = date_format(date_create($valor->data), 'd/m/Y');
                                $intensi = getInte($valor->intensidade);
                                $p1c= convB($valor->p1);
                                $p2c= convB($valor->p2);
                               
                                $what = array('\n','\r\n','\r','"',"'", 'www.', '\r','/\s/',chr(10) );
                                $by   = array(' ',' ', ' ',' ',' ','files.',' ',' ', ' <br>');                            
                                $municpio_tratado = str_replace($what, $by, $valor->municipio);                              
                                $localidade = str_replace($what, $by, $valor->nome_localidade); 

                                $obsTratada = htmlentities($valor->obs, null, 'utf-8');     

                                $obsTratada = str_replace($what, $by, $obsTratada);     
                                
                                if($valor->nome_localidade != null){
                                    $localidade_tratada = "<br> $localidade - $valor->municipio - $valor->uf";
                                  } else {
                                    $localidade_tratada = "<br> $valor->municipio - $valor->uf";
                                  }
                                  
                                  if(strpos($valor->img, '|') != ""){
                                      $n = strpos($valor->img, ' |');
                                      $urlTratado = substr($valor->img,0,$n);
                                  } else {
                                    $urlTratado = $valor->img;
                                  }
                                  
                                  $urlTratado = str_replace($what, $by, $urlTratado);  
    

                             
                              if($valor->nome_localidade != null){
                                $localidade_tratada = "<br> $localidade - $valor->municipio - $valor->uf";
                              } else {
                                $localidade_tratada = "<br> $valor->municipio - $valor->uf";
                              }

  							echo "{ lat: $valor->latitude, lng: $valor->longitude, icone:$valor->intensidade, info: \" <b> ID: </b> $valor->id - $dataformatada  - $valor->hora <br> <b> $intensi </b> $localidade_tratada  <br> <b>Coordenadas: </b> $valor->latitude, $valor->longitude <br> <b> Limpeza concluída?:</b> $p1c <br> <b> Equipe no local?</b> $p2c  <br> <b> Obs:</b> $obsTratada   <br>  <img src='$urlTratado' width='200px'  />    \" }, ";
            
                              echo "\n \n";  
                        }
						?>

    ];

                        //google.maps.event.addDomListener(window, "load", initMap);
</script>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARsZAdKJtlMX_6g83BzAMkY4CGPtbu05k&libraries=visualization&callback=initMap">
</script>

</body>
</html>