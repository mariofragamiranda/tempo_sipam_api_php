<?php
include 'pre_topo.php';

$listaEstados  = $OleoAppControl->findEstados();


?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="sticky-footer-navbar.css" rel="stylesheet">
    <link href="offcanvas.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>




</head>

<body class="d-flex flex-column h-100 bg-light">

    <header class="fixed-top">
        <?php include 'topo.php'; ?>
    </header>


    <div class="container-fluid" id="container-olho">
        <div class="row">

            <?php include 'menu.php'; ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">


                <h2 class="mt-4"><?php echo("Pesquisa Avançada") ?></h2> <br>
                <h4 class="mt-4"><?php echo("Pesquisa Direta por ID") ?></h4> <br>

                <form class="form-inline" action="index.php" method="get">
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="hidden" name="buscaAvancada" value="1" />
                        <input type="text" class="form-control" id="id" name="id" placeholder="ID">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Carregar no Mapa</button>
                </form>


                <h4 class="mt-4">Pesquisa com Vários Filtros</h4> <br>

                <form  action="index.php" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="inputEmail4">Data Inicial:</label>
                            <input type="hidden" name="buscaAvancada" value="2" />
                            <input class="form-control mr-sm-2" size="8" maxlength="10" type="text" name="dataConsulta"
                                value="<?php echo($dataCampoBusca); ?>">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputPassword4">Data Final:</label>
                            <input class="form-control mr-sm-2" size="8" maxlength="10" type="text"
                                name="dataConsultaFinal" value="<?php echo($dataCampoBuscaFinal); ?>">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">Situação</label>
                            <select id="inputState" class="form-control" name="status">
                                <option value="0">Todas a Situações</option>
                                <option value="1">Oleada - Manchas</option>
                                <option value="2">Oleada - Vestígios / Esparsos</option>
                                <option value="3">Óleo Não Observado</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">Estado</label>
                            <select id="inputState" class="form-control" name="uf">
                                <option value="0" selected>Todos os Estados</option>

                                <?php 
    	foreach($listaEstados as $item){
    echo("<option value='$item->uf'> $item->estado </option>"); 
        }?>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-2">
                            <label for="inputState">Limpeza Concluída</label>
                            <select id="inputState" class="form-control" name="p1">
                                <option value="2" selected>Todas as Sitauções</option>
                                <option value="1">Sim</option>
                                <option value="0">Não</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">Equipe no Local</label>
                            <select id="inputState" class="form-control" name="p2">
                                <option value="2" selected>Todas as Sitauções</option>
                                <option value="1">Sim</option>
                                <option value="0">Não</option>
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Carregar Mapa</button>
                </form>


                <h4 class="mt-4">Pesquisa com Vários Filtros - Mapa de Densidade</h4> <br>

<form  action="kernel.php" method="get">
    <div class="form-row">
        <div class="form-group col-md-2">
            <label for="inputEmail4">Data Inicial:</label>
            <input type="hidden" name="buscaAvancada" value="2" />
            <input class="form-control mr-sm-2" size="8" maxlength="10" type="text" name="dataConsulta"
                value="<?php echo($dataCampoBusca); ?>">
        </div>
        <div class="form-group col-md-2">
            <label for="inputPassword4">Data Final:</label>
            <input class="form-control mr-sm-2" size="8" maxlength="10" type="text"
                name="dataConsultaFinal" value="<?php echo($dataCampoBuscaFinal); ?>">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputCity">Situação</label>
            <select id="inputState" class="form-control" name="status">
                <option value="0">Todas a Situações</option>
                <option value="1">Oleada - Manchas</option>
                <option value="2">Oleada - Vestígios / Esparsos</option>
                <option value="3">Óleo Não Observado</option>

            </select>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputCity">Estado</label>
            <select id="inputState" class="form-control" name="uf">
                <option value="0" selected>Todos os Estados</option>

                <?php 
foreach($listaEstados as $item){
echo("<option value='$item->uf'> $item->estado </option>"); 
}?>
            </select>
        </div>
    </div>

    <div class="form-row">

        <div class="form-group col-md-2">
            <label for="inputState">Limpeza Concluída</label>
            <select id="inputState" class="form-control" name="p1">
                <option value="2" selected>Todas as Sitauções</option>
                <option value="1">Sim</option>
                <option value="0">Não</option>
            </select>
        </div>
        <div class="form-group col-md-2">
            <label for="inputZip">Equipe no Local</label>
            <select id="inputState" class="form-control" name="p2">
                <option value="2" selected>Todas as Sitauções</option>
                <option value="1">Sim</option>
                <option value="0">Não</option>
            </select>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Carregar Mapa</button>
</form>


            </main>

        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

</body>

</html>