<?php
include 'pre_topo.php';


$listaPontos1 = $OleoAppControl->findRelatorioStatusUF($dataConsulta, $dataConsultaFinal);



?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="sticky-footer-navbar.css" rel="stylesheet">

    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>

    


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);



    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Estado');
      data.addColumn('number', 'Oleo Nao Observado');
      data.addColumn('number', 'Oleada - Manchas');
      data.addColumn('number', 'Oleada - Vestigios / Esparsos');      

      data.addRows([

        <?php
      foreach($listaPontos1 as $item){ 
        echo("['$item->estado', $item->inte_3, $item->inte_1, $item->inte_2 ], \n"); }
    ?>
      ]);


      

      var options = {
          chart: {
            title: 'Status por Estados',
            subtitle: ' ',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
  </script>


</head>

<body class="d-flex flex-column h-100 bg-light">

    <header class="fixed-top">
        <?php include 'topo.php'; ?>
    </header>


    <div class="container-fluid" id="container-olho">
        <div class="row">

        <?php include 'menu.php'; ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

                <h2 class="mt-4"><?php echo("Relatório por Status nos Estados no Período de $dataCampoBusca até $dataCampoBuscaFinal") ?></h2>
                <br>

                <div id="columnchart_material" style="width: 1200px; height: 650px;"></div>

            </main>

        </div>
    </div>





    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

</body>

</html>