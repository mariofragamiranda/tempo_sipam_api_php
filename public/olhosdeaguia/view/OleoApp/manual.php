<?php
include 'pre_topo.php';


?>

<!DOCTYPE html>
<html lang="pt-br" class="h-100">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="sticky-footer-navbar.css" rel="stylesheet">
    <link href="offcanvas.css" rel="stylesheet">
    <link href="dashboard.css" rel="stylesheet">
    <title>Olhos de Águia - Manchas de Óleo - SIPAM</title>

    


</head>

<body class="d-flex flex-column h-100 bg-light">

    <header class="fixed-top">
        <?php include 'topo.php'; ?>
    </header>


    <div class="container-fluid" id="container-olho">
        <div class="row">

        <?php include 'menu.php'; ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

            <p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://appportalmeteorologia.sipam.gov.br/images/img01.JPG" alt="" width="609" height="137" /></p>
<h2 style="text-align: center;">Orienta&ccedil;&otilde;es para Preenchimento dos Relat&oacute;rios de Vistoria - Aplicativo Olhos de &Aacute;guia</h2>
<br>
<p style="text-align: justify;">A partir do dia 20/nov todos colaboradores devem utilizar o sistema Olhos de &Aacute;guia. Para que a migra&ccedil;&atilde;o seja feita de forma consistente, excepcionalmente na quarta feira dia 20/nov solicitamos que os registros sejam feitos em ambos aplicativos (JotForm e Olhos de &Aacute;guia). Ao final de quarta feira informaremos se a migra&ccedil;&atilde;o foi feita com sucesso para ent&atilde;o adotarmos apenas um aplicativo.</p>
<p style="text-align: justify;">O aplicativo foi desenvolvido pelo SIPAM e est&aacute; dispon&iacute;vel para download atrav&eacute;s no link:</p>
<p style="text-align: center;"><strong><a href="http://appportalmeteorologia.sipam.gov.br/manchas.apk" target="_blank" rel="noopener">http://appportalmeteorologia.sipam.gov.br/manchas.apk</a></strong></p>
<p style="text-align: justify;"><br />Caso o dispositivo apresente erro ao clicar no link acima, copie e cole o link num browser. Ap&oacute;s a instala&ccedil;&atilde;o, fa&ccedil;a seu cadastro informando nome, institui&ccedil;&atilde;o, e-mail e telefone. O aplicativo n&atilde;o precisa de cobertura da rede de telefonia para funcionar. Caso o registro em campo seja feito em uma regi&atilde;o sem cobertura, as informa&ccedil;&otilde;es ficar&atilde;o registradas no celular. O envio das informa&ccedil;&otilde;es s&oacute; ocorre quando o usu&aacute;rio realizar o procedimento de sincroniza&ccedil;&atilde;o (menu &gt; sincronizar dados). Esse procedimento pode ser feito quando houver cobertura da rede de celular ou conex&atilde;o a uma rede wi-fi.</p>
<p style="text-align: justify;">N&atilde;o h&aacute; necessidade de preencher o nome da localidade, munic&iacute;pio e estado. Essas informa&ccedil;&otilde;es ser&atilde;o preenchidas automaticamente ap&oacute;s a sincroniza&ccedil;&atilde;o dos dados. No entanto &eacute; aconselh&aacute;vel que no campo &ldquo;Observa&ccedil;&otilde;es&rdquo; seja preenchido o nome da localidade sobretudo para que se possa identificar a ocorr&ecirc;ncia de &oacute;leo em novas localidades. Caso essa informa&ccedil;&atilde;o n&atilde;o seja preenchida, a equipe de elabora&ccedil;&atilde;o do mapa do GAA preencher&aacute; o nome da localidade que estiver dispon&iacute;vel nas bases do OpenStreetMap ou do Google Maps.</p>
<p style="text-align: justify;">Importante destacar que o conceito de localidade utilizado no mapeamento se restringe a uma &aacute;rea de 1km ao longo da costa. Portanto uma praia com uma faixa de areia com 10km possui 10 localidades com o mesmo nome. Dessa forma a metodologia representar&aacute; melhor a realidade. Para a defini&ccedil;&atilde;o da situa&ccedil;&atilde;o atual da praia, considere os seguintes exemplos como par&acirc;metro de refer&ecirc;ncia. </p>


<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://appportalmeteorologia.sipam.gov.br/images/img02.JPG" width="850" height="392" /></p>
<p style="text-align: justify;">Em caso de &Oacute;leo n&atilde;o observado, coloque sempre ao menos uma foto para cada localidade, sendo uma vista panor&acirc;mica ao longo da praia (com vis&atilde;o do horizonte). Se houver &oacute;leo no local coloque tamb&eacute;m uma foto do &oacute;leo com refer&ecirc;ncia de escala (ex: caneta) e uma foto de uma se&ccedil;&atilde;o (ex: foto na dire&ccedil;&atilde;o do mar). Diferente do JotForm, no Aplicativo Olhos de &Aacute;guia, cada registro comporta apenas uma foto, no entanto, poder&atilde;o ser enviadas quantas fotos forem necess&aacute;rias para cada localidade. Se houver conflito nas respostas para a mesma localidade, ser&aacute; adotada a reposta mais conservadora, por ex.: &ldquo;Oleada &ndash; manchas&rdquo; em detrimento a &ldquo;Oleada &ndash; vest&iacute;gios esparsos&rdquo;.</p>
<p style="text-align: justify;">Seguem exemplos:</p>
<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://appportalmeteorologia.sipam.gov.br/images/img02.JPG" width="850" height="392" /></p>
<p>Qualquer d&uacute;vida pode ser sanada via grupos WhatsApp ou pelo e-mail <strong><a href="mailto:grupoacompanhamentoavaliacao@gmail.com" target="_blank" rel="noopener">grupoacompanhamentoavaliacao@gmail.com</a></strong></p>

            </main>

        </div>
    </div>





    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

</body>

</html>