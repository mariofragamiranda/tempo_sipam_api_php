<?php 

if(isset($_POST['senha']) )
{
  if(strtolower($_POST['senha']) == "sipam" || strtolower($_POST['senha']) == "admoperacaoaa" ){
    session_start();
    $_SESSION["logado"]="logado";

    if(strtolower($_POST['senha']) == "admoperacaoaa" ){
      $_SESSION["adm"]="adm";
    }

    header('Location:index.php');
  } else {
    header('Location:login.php?msgErro=Senha Incorreta!');
  }
}

$msgErro = "";

if(isset($_GET['msgErro'])) {
  $msgErro = $_GET['msgErro'];
}


?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="signin.css" rel="stylesheet">

    <title>Olhos de Águia - Manchas de Óleo</title>
  </head>
  <body>
  <form class="form-signin" action="login.php" method="post">
      <img class="mb-4" src="logo_manchas_ex.png"  alt="">

      <?php if($msgErro != "") { ?>
      <div class="alert alert-danger" role="alert">
      <?php echo($msgErro); ?>
      </div>
      <?php } ?>
      
      <label for="inputPassword" class="sr-only">Senha</label>
      <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" required autofocus>
      <div class="checkbox mb-3">
      
        <!--         
        <label>
          <input type="checkbox" value="remember-me"> Lembrar de mim
        </label>
        -->
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
      <p class="mt-5 mb- text-muted text-black-50">&copy; Sistema de Proteção da Amazônia - 2019</p>
    </form>

    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>