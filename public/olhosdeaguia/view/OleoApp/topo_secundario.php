<div class="nav-scroller bg-white shadow-sm zindex" style="z-index: -1;">
    <nav class="nav nav-underline">
      <a class="nav-link active"  href="#" >Relatórios por: </a>
      <a class="nav-link" href="<?php echo($links['relatoriosEstados']); ?>">Estados</a>
      <a class="nav-link" href="<?php echo($links['relatoriosMunicipios']); ?>">Municípios</a>
      <a class="nav-link" href="<?php echo($links['relatorioStatusPeriodo']); ?>">Status por Período</a>
      <a class="nav-link" href="<?php echo($links['relatorioStatusEstado']); ?>">Status por Estado</a>
      <a class="nav-link" href="<?php echo($links['relatorioSituacaoOcorrencia']); ?>">Ocorrências de Óleo</a>
      <a class="nav-link" href="<?php echo($links['pesquisaAvancada']); ?>">Pesquisa Avançada</a>
      <a class="nav-link" href="<?php echo($links['csv']); ?>">Baixar Arquivo CSV</a>
      
     <!-- <a class="nav-link" href="#Status">Status</a> -->
    </nav>
  </div>