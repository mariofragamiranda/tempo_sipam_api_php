<?php


include '../../control/OleoAppControl.php';
include 'uteis.php';

$OleoAppControl = new OleoAppControl();

session_start();

if($_SESSION["logado"] != "logado"){
    header('Location: '.'login.php');
}

$isAdm = false;

if( isset($_SESSION["adm"])){
  $isAdm = true;
}


$dataCampoBusca = "";
$dataCampoBuscaFinal = "";

$complemento_links = "";

$links = array();
$links['index']  = "index.php".$complemento_links;
$links['csv']  = "csv.php".$complemento_links;
$links['relatoriosEstados'] = "relatoriosEstados.php".$complemento_links;
$links['relatoriosMunicipios'] = "relatoriosMunicipios.php".$complemento_links;
$links['relatorioStatusPeriodo'] = "relatorioStatusPeriodo.php".$complemento_links;
$links['relatorioStatusEstado'] = "relatorioStatusEstado.php".$complemento_links;
$links['relatorioSituacaoOcorrencia'] = "relatoriosStatusGeral.php".$complemento_links;
$links['manual'] = "manual.php".$complemento_links;
$links['pesquisaAvancada'] = "pesquisaAvancada.php".$complemento_links;

if(empty($_GET['dataConsulta'])) {
  
  $dataCampoBusca = date('d/m/Y',strtotime("-2 day"));
  $dataCampoBuscaFinal = date('d/m/Y');

 $dataConsulta =DateTime::createFromFormat('d/m/Y', date('d/m/Y', strtotime("-2 day")));
 $dataConsultaFinal =DateTime::createFromFormat('d/m/Y', date('d/m/Y'));


} else {
 $dataConsulta =DateTime::createFromFormat('d/m/Y', $_GET['dataConsulta']);
 $dataConsultaFinal =DateTime::createFromFormat('d/m/Y', $_GET['dataConsultaFinal']);

 //exit();

 $dataCampoBusca = $dataConsulta->format('d/m/Y');
 $dataCampoBuscaFinal = $dataConsultaFinal->format('d/m/Y');

 $complemento_links = "?dataConsulta={$dataCampoBusca}&dataConsultaFinal={$dataCampoBuscaFinal}";

 $links['index']  = "index.php".$complemento_links;
 $links['csv']  = "csv.php".$complemento_links;
 $links['relatoriosEstados'] = "relatoriosEstados.php".$complemento_links;
 $links['relatoriosMunicipios'] = "relatoriosMunicipios.php".$complemento_links;
 $links['relatorioStatusPeriodo'] = "relatorioStatusPeriodo.php".$complemento_links;
 $links['relatorioStatusEstado'] = "relatorioStatusEstado.php".$complemento_links;
 $links['relatorioSituacaoOcorrencia'] = "relatoriosStatusGeral.php".$complemento_links;
 $links['manual'] = "manual.php".$complemento_links;
 $links['pesquisaAvancada'] = "pesquisaAvancada.php".$complemento_links;

}

$coordenadasCentrais = "-10.071667, -35.271126";


if(empty($_GET['buscaAvancada'])) {
  $listaPontos = $OleoAppControl->findAllIntervalo($dataConsulta, $dataConsultaFinal);
} else {
  if($_GET['buscaAvancada'] == 1){
  $listaPontos = $OleoAppControl->findId($_GET['id']);
    if(count($listaPontos) > 0){
     $coordenadasCentrais = strval($listaPontos[0]->latitude). ', ' . strval($listaPontos[0]->longitude);
    }    
  } 
  if($_GET['buscaAvancada'] == 2){

    $v1 = $_GET['status'];
    $v2 = $_GET['uf'];
    $vp1 = $_GET['p1'];
    $vp2 = $_GET['p2'];

    if($v1 == "0"){
      $sqlP1 = " ";
    } else {
      $sqlP1 = " (intensidade = $v1) and ";
    }

    if($v2 == "0"){
      $sqlP2 = " ";
    } else {
      $sqlP2 = " (uf = '$v2') and  ";
    }

    if($vp1 == "2"){
      $sqlP3 = " ";
    } else {
      if($vp1 == "1"){
        $sqlP3 = " (p1 = true) and  ";
      } else {
        $sqlP3 = " (p1 = false) and ";
      }     
    }

    if($vp2 == "2"){
      $sqlP4 = " ";
    } else {
    //  echo ($vp2);
      if($vp2 == "1"){
        $sqlP4 = " (p2 = true) and ";
      } else {
        $sqlP4 = " (p2 = false) and ";
      }     
    }

    $complemento_sql = $sqlP1. ' ' . $sqlP2 .' '. $sqlP3. ' '. $sqlP4;    
   // echo $complemento_sql;  exit;
    $listaPontos = $OleoAppControl->findAvancada($dataConsulta, $dataConsultaFinal, $complemento_sql);
  }
  

}



?>