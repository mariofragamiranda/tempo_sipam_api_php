<?php 
//echo "importou ok";
function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }


   $what = array( '\n','\r\n','\r','"',"'", "www." );
   $by   = array( ' ',' ', ' ',' ',' ','files.' );
    
    $what2 = array( '\n','\r\n','\r','"',"'",',');
    $by2   = array( ' ',' ', ' ',' ',' ','.' ); 
    
    


   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array('id', 'latitude', 'longitude','data', 'hora', 'status', 'obs', 'Nome_Informante', 'Cpf_Informante', 'Orgao_Informante', 'Email_Informanten', 'Telefone_Informante', 'Limpeza_concluida', 'Equipe_no_local', 'uf', 'municipio', 'nome_localidade','localidade_id','Foto'));
   foreach ($array as $valor) {
    $dataformatada = date_format(date_create($valor->data), 'd/m/Y');
    $intensi = getInte($valor->intensidade);
    $p1c= convB($valor->p1);
    $p2c= convB($valor->p2);
    $municpio_tratado = str_replace($what, $by, $valor->municipio);                              
    $localidade = str_replace($what, $by, $valor->nome_localidade);   

    $obsTratada = str_replace($what2, $by2, $valor->obs);   

    if(strpos($valor->img, '|') != ""){
      $n = strpos($valor->img, ' |');
      $urlTratado = substr($valor->img,0,$n);
  } else {
    $urlTratado = $valor->img;
  }

  $cpfTratado = str_replace($what, $by, $valor->cpf);

    fputcsv($df, array($valor->id, $valor->latitude, $valor->longitude, $dataformatada, $valor->hora, $intensi, $obsTratada, $valor->nome_informante, $cpfTratado,  $valor->orgao_informante, $valor->email_informante,  $valor->telefone_informante, $p1c, $p2c, $valor->uf, $municpio_tratado, $localidade, $valor->localidade_id, $urlTratado));
   }
   fclose($df);
   return ob_get_clean();
}


Function convB($b){
    if($b){
      return "Sim";
    } else {
      return "Nao";
    }
  }

function getInte($inte)
{
    if ($inte == 1) {
        return "Oleada - Manchas";
    }
    if ($inte == 2) {
        return "Oleada - Vestigios / Esparsos";
    }
    if ($inte == 3) {
        return "Oleo Nao Observado";
    }
}



function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header('Content-Encoding: UTF-8');
    header('Content-Type: text/html; charset=utf-8');

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}


?>