<?php
include '../../model/OleoApp.php';

class OleoAppControl{
	
	function findAll($dataConsulta){
		$oleos = new OleoApp();
		return $oleos->findAll($dataConsulta);
	}	

	function findAllIntervalo($dataConsulta, $dataConsultaFinal){
		$oleos = new OleoApp();
		return $oleos->findAllIntervalo($dataConsulta, $dataConsultaFinal);
	}	


	function findId($id){
		$oleos = new OleoApp();
		return $oleos->findId($id);
	}	

	function findAvancada($dataConsulta, $dataConsultaFinal, $complemento_sql){
		$oleos = new OleoApp();
		return $oleos->findAvancada($dataConsulta, $dataConsultaFinal, $complemento_sql);
	}	


	function findRelatorioEstadosOleo($dataConsulta, $dataConsultaFinal, $status){
		$oleos = new OleoApp();
		return $oleos->findRelatorioEstadosOleo($dataConsulta, $dataConsultaFinal, $status);
	}	

	function findRelatorioMunicipioStatus($dataConsulta, $dataConsultaFinal, $status){
		$oleos = new OleoApp();
		return $oleos->findRelatorioMunicipioStatus($dataConsulta, $dataConsultaFinal, $status);
	}	

	function findRelatorioStatusPeriodo($dataConsulta, $dataConsultaFinal){
		$oleos = new OleoApp();
		return $oleos->findRelatorioStatusPeriodo($dataConsulta, $dataConsultaFinal);
	}

	function findRelatorioStatusUF($dataConsulta, $dataConsultaFinal){
		$oleos = new OleoApp();
		return $oleos->findRelatorioStatusUF($dataConsulta, $dataConsultaFinal);
	}
	
	function findRelatorioStatusGeral($dataConsulta, $dataConsultaFinal){
		$oleos = new OleoApp();
		return $oleos->findRelatorioStatusGeral($dataConsulta, $dataConsultaFinal);
	}

	function findEstados(){
		$oleos = new OleoApp();
		return $oleos->findEstados();
	}
	

	function updateDadosPonto($id, $intensidade, $obs, $excluido){
		$oleos = new OleoApp();
		return $oleos->updateDadosPonto($id, $intensidade, $obs, $excluido);
	}
}

?>