<?php
include '../../model/PontoSatelite.php';

class PontoSateliteControl{
	
	function findAll($uf, $satelite, $dataConsulta){
		$pontSatelite = new PontoSatelite();
		return $pontSatelite->findAll($uf, $satelite, $dataConsulta);
	}

	function findTi($dataConsultaInicio, $dataConsultaFim, $idTi){
		$pontSatelite = new PontoSatelite();
		return $pontSatelite->findTi($dataConsultaInicio, $dataConsultaFim, $idTi);
	}
}

?>