
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Radares da Amazônia - Sipam</title>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARsZAdKJtlMX_6g83BzAMkY4CGPtbu05k&libraries=visualization"></script>	
	<!--
	<script type="text/javascript" src="dsajs/manaus.js"></script>
	-->
	<style>
    *{
    padding: 0px;
    margin: 0px;
    }
      html, body {
        height: 100vh;
        margin: 0px;
        padding: 0px
      }

      #legenda
        {
            position: absolute;
            bottom: 50px;  /* adjust value accordingly */
            left: 10px;  /* adjust value accordingly */
            opacity: 0.9;
            filter: alpha(opacity=90);
            z-index:9999;
        }
       #dataanima
       {
            position: absolute;
            top: 13px;  /* adjust value accordingly */
            right: 10px;  /* adjust value accordingly */
            opacity: 0.9;
            filter: alpha(opacity=90);
            z-index:9999;
            font-weight:bold;
            text-align:right;
            fonte-size:9px;
       }

        #rodape
        {
            position: absolute;
            bottom: 15px;  /* adjust value accordingly */
            left: 10px;  /* adjust value accordingly */            
            z-index:9998;
            width: 98%;
            opacity: 0.9;
            filter: alpha(opacity=90);
        }
      #anima #mapaanimacao
      {
        height: 100vh;
        z-index:1;
        width: 100%;
       }

    input[type="checkbox"]{
        width: 30px; /*Desired width*/
        height: 30px; /*Desired height*/
        cursor: pointer;
        -webkit-appearance: none;
        appearance: none;
    }

    </style>
    
	
	<script>
    var cur_frame=0, frame_input;
    var opacity = 0.5, f_break = 700, f_speed = 1000,  reverse = 0;
    var timeout, overlay, titulo;
    var imgMunicipio;

    var coordsBelem = new google.maps.LatLngBounds( new google.maps.LatLng(-3.5581,-50.6343), new google.maps.LatLng(0.7468,-46.3250));
    var coordsBoaVista = new google.maps.LatLngBounds( new google.maps.LatLng(0.6897,-62.8610), new google.maps.LatLng(4.9946,-58.5477));	
    var coordsCruzeiroSul = new google.maps.LatLngBounds( new google.maps.LatLng(-9.7426,-74.9619), new google.maps.LatLng(-5.4377,-70.6157));	
    var coordsMacapa = new google.maps.LatLngBounds( new google.maps.LatLng(-2.1069,-53.2510), new google.maps.LatLng(2.1980,-48.9430));
    var coordsManaus = new google.maps.LatLngBounds( new google.maps.LatLng(-5.2991,-62.1442), new google.maps.LatLng(-0.9942 ,-57.8297));
    var coordsSaoGabriel = new google.maps.LatLngBounds( new google.maps.LatLng(-2.2960,-69.2107), new google.maps.LatLng(2.0090,-64.9027));
    var coordsPortoVelho = new google.maps.LatLngBounds( new google.maps.LatLng(-10.8615,-66.0605), new google.maps.LatLng(-6.5566,-61.7021));	
    var coordsSantarem = new google.maps.LatLngBounds( new google.maps.LatLng(-4.5805,-56.9514), new google.maps.LatLng(-0.2755,-52.6395));	
    var coordsSaoLuis = new google.maps.LatLngBounds( new google.maps.LatLng(-4.7512,-46.3920),  new google.maps.LatLng(-0.4463,-42.0796));	
    var coordsTabatinga = new google.maps.LatLngBounds( new google.maps.LatLng(-6.3978,-72.0889), new google.maps.LatLng(-2.0929,-67.7690));
    var coordsTefe = new google.maps.LatLngBounds( new google.maps.LatLng(-5.5229,-66.8460), new google.maps.LatLng(-1.2179,-62.5305));	
   /** var coordsGoes = new google.maps.LatLngBounds( new google.maps.LatLng(-21.280718, -82.004587), new google.maps.LatLng(8.630975, -34.024259));
    
    var coordsGoes = new google.maps.LatLngBounds(
        new google.maps.LatLng(-21.280718, -82.004587),
        new google.maps.LatLng(8.630975, -34.024259));
    
    var coordsGoes = new google.maps.LatLngBounds( new google.maps.LatLng(-18.61, -82.00), new google.maps.LatLng(8.46, -34.19));	
    var coordsMunicipios = new google.maps.LatLngBounds( new google.maps.LatLng(-14.978265, -73.819772), new google.maps.LatLng(5.537757, -39.893992));
*/

    var pontoCentralGoes = new google.maps.LatLng(-3.12369, -60);	
    var pontoCentralBelem = new google.maps.LatLng(-1.4066666666666665, -48.48166666666667);
    var pontoCentralBoaVista = new google.maps.LatLng(2.8441666666666667, -60.70027777777778);	
    var pontoCentralCruzeiroSul = new google.maps.LatLng(-7.595555555555555, -72.79972222222221);	
    var pontoCentralPortoVelho = new google.maps.LatLng(-8.715277777777777, -63.89388888888889);	
    var pontoCentralSantarem = new google.maps.LatLng(-2.429722222222222, -54.79888888888889);
    var pontoCentralTabatinga = new google.maps.LatLng(-4.248333333333333, -69.93500);
    var pontoCentralSaoLuis = new google.maps.LatLng(-2.6005555555555557, -44.239444444444445);	
    var pontoCentralMacapa = new google.maps.LatLng(0.04555555555555556, -51.096944444444446);
    var pontoCentralManaus = new google.maps.LatLng(-3.12369, -60);
    var pontoCentralTefe = new google.maps.LatLng(-3.372777777777778, -64.69305555555556);
    var pontoCentralSaoGabriel = new google.maps.LatLng(-0.1436111111111111, -67.05694444444444);

    var QTD_IMG_ANIMACAO=6;
    var map,mapAnimacao;

    //Get Last Data from cappi file
    function getLastDate(strDate){
        var res = strDate.data.split(" ", 2);
        var dt = res[0];	
        var hr = res[1].split(".",1);	
        dt = dt.replace("-", "");
        dt = dt.replace("-", "");
        hr = hr[0];
        hr=hr.replace(":", "");
        hr=hr.replace(":", "");			
        var dthr = parseInt (dt +hr);	
    return dthr;
    }

    function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
        if (httpRequest.status === 200) {
            var data = JSON.parse(httpRequest.responseText);
            if (callback) callback(data);
        }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
    }

    function formataData(data){

        //now.getDate() + " de " + monName [now.getMonth() ]   +  " de "  +     now.getFullYear () 
       // return databruta.toString();
      
        dia  = data.getDate().toString();
        diaF = (dia.length == 1) ? '0'+dia : dia;
        mes  = (data.getMonth()+1).toString(); //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length == 1) ? '0'+mes : mes;
        anoF = data.getFullYear();

        hora = data.getHours();
        min = data.getMinutes();

        horaF = (dia.hora == 1) ? '0'+hora : hora;
        minF =  (min.hora == 1) ? '0'+min : min;

        return diaF+"/"+mesF+"/"+anoF + " - " + horaF + ":" + minF; 

    }

    //Busca imagens para animacao (limitado até 10 imagens)
    function buscaImagens(variavel,fonte,formato,coords) {
        fonte = fonte.toLowerCase();
        if(fonte == 'cruzeirodosul'){
            fonte = 'cruzeirosul';
        }
        //$.getJSON('http://sosamazonia.sipam.gov.br/sosmanaus/getimagens.json?variavel=' + variavel + "&fonte=" + fonte + "&formato=" + formato, function(j){
            fetchJSONFile('http://appportalmeteorologia.sipam.gov.br/radares/view/dados/index.php?fonte='+fonte, function(j){	
            var qtd = j.length;	
            //print(j);
            if (qtd > QTD_IMG_ANIMACAO)
                qtd = QTD_IMG_ANIMACAO;
            if (overlay != null && overlay.length > 1)
                for (var i=0; i<overlay.length; i++) { hideOverlay(i); }
            overlay = new Array(1);
            titulo = new Array(1);
            cur_frame=0; 
            var aux = 0;                   
            var iniPos = j.length < QTD_IMG_ANIMACAO? 0 : j.length - QTD_IMG_ANIMACAO;
                var indice = 0;		
            for (var i=iniPos;i<j.length;i++) {
                overlay[indice] = new google.maps.GroundOverlay("http://appportalmeteorologia.sipam.gov.br/radares/view/camadas/"+j[i].url, coords, { clickable: false });
                
                var  dataTratada = new Date(j[i].data + " UTC");
                
                titulo[indice] = "Precipitação Instantânea: <br>" + formataData(dataTratada);
                aux++;
                indice ++;
            }
            if (overlay.length > 1)
                startLoop(0);
        });
    }
    //Anima as imagens de radar
    function loopRadar(sf,loop) {
        for (var i=0; i<overlay.length; i++) { hideOverlay(i); }
        showOverlay(sf);
        document.getElementById("dataanima").innerHTML = titulo[sf];
        cur_frame = sf;
        if (reverse) {
            sf--;
            if (sf == 0) { sf = overlay.length - 1; var td = f_break; }
            else { var td = f_speed; }
        } else {
            sf++;
            if (sf >= overlay.length) {sf = 0; var td = f_break; }
            else { var td = f_speed; }
        }
        if (loop) {    
            var f = function() { loopRadar(sf,1); };
            timeout = setTimeout(f, td);
        }
    }
    //Muda opacidade da imagem na animacao
    function showOverlay(ind) {
        overlay[ind].setMap(mapAnimacao);
        overlay[ind].setOpacity(0.6);
    }
    //Remove imagem da animacao
    function hideOverlay(ind) {
        overlay[ind].setMap();
    }
    //Para a animacao
    function stopLoop() {
        clearTimeout(timeout);
    }
    //Inicia a animacao
    function startLoop(rev) {
        clearTimeout(timeout);
        reverse = rev;
        loopRadar(cur_frame,1);
    }
    //Passa para a proxima imagem da animacao
    function forwardStep() {
        clearTimeout(timeout);
        if (++cur_frame >= overlay.length) { cur_frame = 0; }
        loopRadar(cur_frame,0);
    }
    //Para para a imagem anterior da animacao
    function reverseStep() {
        clearTimeout(timeout);
        if (--cur_frame < 0) { cur_frame = overlay.length-1; }
        loopRadar(cur_frame,0);
    }
    //Monta mapa da animacao
    function animacao(variavel,fonte,formato,pontoCentral,coords, mostrarLocal, latLocal, longLocal) {	
        var ponto =  new google.maps.LatLng(pontoCentral);
        var z = 7;
        var myOptions = {
            center: pontoCentral, zoom: z, mapTypeId: google.maps.MapTypeId.TERRAIN, fullscreenControl: false, zoomControl: true, mapTypeControl: true, streetViewControl: false, disableDefaultUI:true };
        mapAnimacao = new google.maps.Map(document.getElementById("mapaanimacao"), myOptions);

        if(mostrarLocal = true){
            var meulocal = {lat: latLocal, lng: longLocal};
            var marker = new google.maps.Marker({position: meulocal, icon:"icone_localizacao.png",  map: mapAnimacao});

        }


        
        buscaImagens(variavel,fonte,formato,coords);
    }



    var circulo1;
    var circulo2;
    var circulo3;
    var circulo4;
    var circulo5;
    var linhas = false;

    function myFunction() {

        // If the checkbox is checked, display the output text
        if (linhas == false){
            var cor_radar = '#FF0000';
            circulo1  = new google.maps.Circle({strokeColor: cor_radar, strokeOpacity: 0.2, strokeWeight: 1.5,
                fillColor: cor_radar, fillOpacity: 0.01,map: mapAnimacao, center: <?php echo $_GET['pontoCentral'];?>, radius: 250000 });

            circulo2  = new google.maps.Circle({strokeColor: cor_radar, strokeOpacity: 0.2, strokeWeight: 1.5,
                fillColor: cor_radar, fillOpacity: 0.01,map: mapAnimacao, center:  <?php echo $_GET['pontoCentral'];?>, radius: 200000 });

            circulo3  = new google.maps.Circle({strokeColor: cor_radar, strokeOpacity: 0.2, strokeWeight: 1.5,
                fillColor: cor_radar, fillOpacity: 0.01,map: mapAnimacao, center:  <?php echo $_GET['pontoCentral'];?>, radius: 150000 });

            circulo4  = new google.maps.Circle({strokeColor: cor_radar, strokeOpacity: 0.2, strokeWeight: 1.5,
                fillColor: cor_radar, fillOpacity: 0.01,map: mapAnimacao, center:  <?php echo $_GET['pontoCentral'];?>, radius: 100000 });

            circulo5  = new google.maps.Circle({strokeColor: cor_radar, strokeOpacity: 0.2, strokeWeight: 1.5,
                fillColor: cor_radar, fillOpacity: 0.01,map: mapAnimacao, center:  <?php echo $_GET['pontoCentral'];?>, radius: 50000 });
            linhas = true;

        } else {

            linhas = false;
            circulo1.setMap(null);
            circulo2.setMap(null);
            circulo3.setMap(null);
            circulo4.setMap(null);
            circulo5.setMap(null);
        }
    }

	</script>



</head>

<body class="easyui-layout" onload="animacao('cappi','<?php echo $_GET['fonte'];?>','png',<?php echo $_GET['pontoCentral'];?>,<?php echo $_GET['coords'];?>,<?php echo $_GET['mostrarLocal'];?>,<?php echo $_GET['latLocal'];?>,<?php echo $_GET['longLocal'];?>);" >

<!-- <body class="easyui-layout" onload="animacao('cappi','portovelho','png',pontoCentralPortoVelho, coordsPortoVelho);" > -->
<!-- http://172.23.14.99:8000/radar.php?fonte=portovelho&pontoCentral=pontoCentralPortoVelho&coords=coordsPortoVelho -->

<div id="legenda">
           <img src="legenda_radar.png" width="177px" />
    </div>

  
    <div id="anima"  title="Animation" data-options="modal:true,closed:true,resizable:true,toolbar:'#dlg-toolbar'">
		<div id="mapaanimacao"></div>
    </div>	
   
    <div id="dataanima" style="font-size: 10px; font-family: Verdana, Geneva, Tahoma, sans-serif"></div>

    <div id="rodape">
		<table>
			<tr>				
				<td style="padding-left:0px;text-align:left;">
					<a href="#" style="margin:0px;padding:0px"><img id="prev" style="margin:0px;padding:0px" border="0" src="dsaimg/anterior.png" alt="previous" onclick="reverseStep()" /></a>
					<a href="#" style="margin:0px;padding:0px"><img id="pause" style="margin:0px;padding:0px" border="0" src="dsaimg/pause.png" alt="pause" onclick="stopLoop()" /></a>
					<a href="#" style="margin:0px;padding:0px"><img id="play" border="0" style="margin:0px;padding:0px" src="dsaimg/player.png" alt="play" onclick="startLoop(0)" /></a>
					<a href="#" style="margin:0px;padding:0px"><img id="next" style="margin:0px;padding:0px" border="0" src="dsaimg/proxima.png"  alt="next" onclick="forwardStep()" /></a>

                    <a onclick="myFunction()" href="#">  <img id="next" style="margin:0px;padding:0px" border="0" src="images/radar.png" width="28px" /> </a>

				</td>			
			</tr>
        </table>
    </div>

    
</body>
</html>
