<?php
include '../../conexao/Conexao.php';

class CamadasRadares extends Conexao{

private $id;
private $variavel;
private $data;
private $url;
private $fonte;

public function buscaImagens($fonte){
    try { 
 
    $sql = "select * from terascan.camadas_radares where fonte = :fonte order by data desc limit 7;";
    $consulta = Conexao::prepare($sql);
    $consulta->bindValue(':fonte', $fonte, PDO::PARAM_STR);
    $consulta->execute();      
    $result = $consulta->fetchAll();
    $result = array_reverse($result);    
    return json_encode($result);
    } catch (Exception $e) { 
              return null;//$e;     
    }  

}


public function processaDados($key){
    set_time_limit(150);
    $fontes = array("belem", "boavista", "cruzeirosul","macapa", "manaus", "portovelho","santarem", "saoluis", "saogabriel", "tabatinga", "tefe");
    foreach ($fontes as &$fonte) {
        $this->salvaImagens($fonte);
    }
}

public function salvaImagens($fonte){    
    try {         
        $formato = "png";
        $variavel = 'cappi';
        $url = "http://sosamazonia.sipam.gov.br/sosmanaus/getimagens.json?variavel=".$variavel."&fonte=".$fonte."&formato=png";
        $json_file = file_get_contents($url);           
        $jsonObj = json_decode($json_file);
        $contador = count($jsonObj);      

      //  var_dump($count);    
        if($contador > 1) {
            rsort($jsonObj);        
            for ($i = 0; $i < 6; $i++) {                   
            $retornoDownload = $this->baixaArquivo('http://sosamazonia.sipam.gov.br/sosmanaus'.$jsonObj[$i]->url,$fonte);
            if($retornoDownload != 'erro'){             
                $this->insert($jsonObj[$i]->data, $variavel, $retornoDownload, $fonte);
            }
            }
        }        
        return "ok";//$json_str;      
    } catch (Exception $e) { 
        return "erro";//$e;     
    }  
}


private function insert($dataHora, $variavel,  $urlCamada, $fonte){
    $sql = 'INSERT INTO terascan.camadas_radares(variavel, "data", "url", "fonte") VALUES (:variavel, :dataHora,:urlCamada, :fonte)';
    $consulta = Conexao::prepare($sql);
    $consulta->bindValue(':dataHora',  $dataHora, PDO::PARAM_STR);
    $consulta->bindValue(':urlCamada', $urlCamada, PDO::PARAM_STR);
    $consulta->bindValue(':variavel' , $variavel, PDO::PARAM_STR); 
    $consulta->bindValue(':fonte' , $fonte, PDO::PARAM_STR); 
        try{
        $consulta->execute();
        echo "ok";
        return "ok";
        } catch (Exception $e) { 
           // print("erro -> ".$e);
          // echo "erro -> ".$e;
            return "erro";//$e;     
        }
}


private function baixaArquivo($urlArquivo, $pasta){
          // Use basename() function to return the base name of file  
        $file_name = basename($urlArquivo);         
        // Use file_get_contents() function to get the file 
        // from url and use file_put_contents() function to 
        // save the file by using base name 
        if(file_put_contents($pasta.'/'.$file_name,file_get_contents($urlArquivo))) { 
        //  echo "File downloaded successfully"; 
            return $pasta.'/'.$file_name;
        } 
        else { 
            echo "File downloading failed."; 
            return "erro";
        }
}

/**
 * Get the value of id
 */ 
public function getId()
{
return $this->id;
}

/**
 * Set the value of id
 *
 * @return  self
 */ 
public function setId($id)
{
$this->id = $id;

return $this;
}

/**
 * Get the value of variavel
 */ 
public function getVariavel()
{
return $this->variavel;
}

/**
 * Set the value of variavel
 *
 * @return  self
 */ 
public function setVariavel($variavel)
{
$this->variavel = $variavel;

return $this;
}

/**
 * Get the value of datahora
 */ 
public function getDatahora()
{
return $this->datahora;
}

/**
 * Set the value of datahora
 *
 * @return  self
 */ 
public function setDatahora($datahora)
{
$this->datahora = $datahora;

return $this;
}

/**
 * Get the value of urlCamada
 */ 
public function getUrlCamada()
{
return $this->urlCamada;
}

/**
 * Set the value of urlCamada
 *
 * @return  self
 */ 
public function setUrlCamada($urlCamada)
{
$this->urlCamada = $urlCamada;

return $this;
}
}

?>