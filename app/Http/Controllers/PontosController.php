<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;



class PontosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getIdUsuario($key, $Email){

        if($key == "ha45664Hk214g5f66l89u11gf"){
            $results = DB::select( DB::raw("SELECT DISTINCT(co_municipio), no_municipio, no_sigla_uf FROM portalmeteorologia.vw_previsao_tempo"));        
        $entidade = new EntidadeRetorno();
        $entidade->listaCidades = $results;
        return json_encode($entidade);
        } else {
            return json_encode(null);
        }

    }

    public function getPontos($key){

        if($key == "ha45664Hk214g5f66l89u11gf"){
        /*    $results = DB::connection('pgsq2')->select( DB::raw("SELECT *  FROM terascan.alvo_aereo ORDER BY ID DESC") );        
        $entidade = new EntidadeRetorno();
        $entidade->retorno = $results;
        return json_encode($entidade);
        */
        
        $results = DB::connection('pgsq2')->select( DB::raw("SELECT terascan.atualiza_geom_full()") );

        return "ok1";
        } else {
            return json_encode(null);
        }

    }


    
        /**
     * @param $key
     * @param $latitude
     * @param $logitude
     * @param $acuracia
     * @param $nome_informante
     * @param $email_informante
     * @param $orgao_informante
     * @return string
     */
    /*
    public function setPonto($key){

        if($key == "ha45664Hk214g5f66l89u11gf"){          
            DB::connection('pgsq2')->table('alvo_aereo')->insert(
                ['data' => $data, 'hora' => $hora, 'latitude' => $latitude, 'longitude' => $longitude, 'acuracia' => $acuracia, 'nome_informante' => $nome_informante, 'email_informante' => $email_informante, 'orgao_informante' => $orgao_informante, 'intensidade' => $intensidade, 'obs' => $obs, ]
            );
            return "ok";
        } else {
            return "erro";
        }

    }
 */
//http://appportalmeteorologia.sipam.gov.br/api/setPonto/ha45664Hk214g5f66l89u11gf/2019-09-05/3:06:58/62.0/-122.084/22.01//-/-/2/0.0/100.0/-
//http://appportalmeteorologia.sipam.gov.br/api/setPonto/ha45664Hk214g5f66l89u11gf/2019-09-05/3:06:58/62.0/-122.084/22.01/-/-/-/2/0.0/100.0/-
//http://appportalmeteorologia.sipam.gov.br/api/setPonto/ha45664Hk214g5f66l89u11gf/2019-09-05/3:06:58 PM/62.0/-122.084/22.01/-/-/-/2/0.0/100.0/-

    
        public function setPonto($key, $data, $hora, $latitude, $longitude, $acuracia, $nome_informante, $email_informante, $orgao_informante, $intensidade, $velocidade, $altitude, $obs){

            $lat1 = (float) $latitude;
            $long = (float) $longitude;

                if($key == "ha45664Hk214g5f66l89u11gf"){
                    DB::connection('pgsq2')->table('terascan.alvo_aereo')->insert(
                        ['data' => $data, 'hora' => $hora, 'latitude' => $lat1, 'longitude' => $long, 'acuracia' => $acuracia, 'nome_informante' => $nome_informante, 'email_informante' => $email_informante, 'orgao_informante' => $orgao_informante, 'intensidade' => $intensidade, 'velocidade' => $velocidade, 'altitude' => $altitude, 'obs' => $obs, ]
                    );


                     /*$idGerado =  DB::connection('pgsq2')->table('terascan.alvo_aereo')->insertGetId(
                        ['data' => $data, 'hora' => $hora, 'latitude' => $lat1, 'longitude' => $long, 'acuracia' => $acuracia, 'nome_informante' => $nome_informante, 'email_informante' => $email_informante, 'orgao_informante' => $orgao_informante, 'intensidade' => $intensidade, 'velocidade' => $velocidade, 'altitude' => $altitude, 'obs' => $obs, ]
                    );*/

                   // DB::connection('pgsq2')->query('insert into terascan.alvo_aereo(data, hora, latitude, longitude, acuracia, nome_informante, email_informante, orgao_informante, intensidade, velocidade, altitude, obs) VALUES (\'2019-09-05\', \'7/10/1996 5:08 PM\', 67.0, -122.084, 22.01, \'a\', \'a\', \'a\', 2, 0.0, 100.0, \'a\');');
                    
                   //DB::connection('pgsq2')->select('select terascan.atualiza_geom_full()');


                    return "ok";    
                } else {
                    return "erro";
                }

    }

    public function setPontoImg($key, $data, $hora, $latitude, $longitude, $acuracia, $nome_informante, $email_informante, $orgao_informante, $intensidade, $velocidade, $altitude, $obs, $img){

        $lat1 = (float) $latitude;
        $long = (float) $longitude;

        if($img != "-"){        
            $img = 'http://appportalmeteorologia.sipam.gov.br/olhosdeaguia/'.$img;
        } else {
            $img = null;
        }
            if($key == "ha45664Hk214g5f66l89u11gf"){
                DB::connection('pgsq2')->table('terascan.alvo_aereo')->insert(
                    ['data' => $data, 'hora' => $hora, 'latitude' => $lat1, 'longitude' => $long, 'acuracia' => $acuracia, 'nome_informante' => $nome_informante, 'email_informante' => $email_informante, 'orgao_informante' => $orgao_informante, 'intensidade' => $intensidade, 'velocidade' => $velocidade, 'altitude' => $altitude, 'obs' => $obs, 'img' => $img, ]
                );


                 /*$idGerado =  DB::connection('pgsq2')->table('terascan.alvo_aereo')->insertGetId(
                    ['data' => $data, 'hora' => $hora, 'latitude' => $lat1, 'longitude' => $long, 'acuracia' => $acuracia, 'nome_informante' => $nome_informante, 'email_informante' => $email_informante, 'orgao_informante' => $orgao_informante, 'intensidade' => $intensidade, 'velocidade' => $velocidade, 'altitude' => $altitude, 'obs' => $obs, ]
                );*/

               // DB::connection('pgsq2')->query('insert into terascan.alvo_aereo(data, hora, latitude, longitude, acuracia, nome_informante, email_informante, orgao_informante, intensidade, velocidade, altitude, obs) VALUES (\'2019-09-05\', \'7/10/1996 5:08 PM\', 67.0, -122.084, 22.01, \'a\', \'a\', \'a\', 2, 0.0, 100.0, \'a\');');
                
               //DB::connection('pgsq2')->select('select terascan.atualiza_geom_full()');


                return "ok";    
            } else {
                return "erro";
            }

}


public function delPonto(Request $request){

    if($request->input('key') == "ha45664Hk214g5f66l89u11gf"){            //  $id = 1100205;
        DB::connection('pgsq2')->update('update terascan.alvo_aereo set excluido = True where latitude=? and longitude =? and data = ? and hora = ?',[$request->input('lat'), $request->input('long'), $request->input('data'), $request->input('hora')]);
        return "ok";
    } else {
        return $request;//$request->input('key');
    }

}


   




    public function setPontoPost(Request $request){



        if($request->input('key') == "ha45664Hk214g5f66l89u11gf"){            //  $id = 1100205;

            /*DB::table('HistoricoGeralConsultas')->insert(
                ['DataHora' => date("Y-m-d H:i:s"), 'IdVeiculo' => $request->input('IdVeiculo'),
                    'Nome' => $request->input('Nome'), 'Telefone' => $request->input('Telefone'),
                    'Cidade' => $request->input('Cidade'), 'UF' => $request->input('UF') ]
            );*/
            return "ok";
        } else {
            return $request->input('key');
        }

    }


       /**
     * @param $key
     * @param $latitude
     * @param $logitude
     * @param $acuracia
     * @param $nome_informante
     * @param $email_informante
     * @param $orgao_informante
     * @return string
     */
    public function setPontoTeste($key, $data, $hora, $latitude, $longitude, $acuracia, $nome_informante, $email_informante, $orgao_informante, $intensidade, $obs){

        if($key == "ha45664Hk214g5f66l89u11gf"){ 
            return "ok";
        } else {
            return "erro123";
        }

    }


}

class EntidadeRetorno {
    public $retorno;        
}
