<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class PrevisaoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get($key, $id){

       // $results = DB::select( DB::raw("SELECT * FROM some_table WHERE some_col = '$someVariable'") );
       //$results = DB::select( DB::raw("SELECT DISTINCT(co_municipio), no_municipio, no_sigla_uf FROM portalmeteorologia.vw_previsao_tempo") );
       $dias;
        if($key == "ha45664Hk214g5f66l89u11gf"){
      //  $id = 1100205;
       $results = DB::select( DB::raw("SELECT dt_data_previsao, co_municipio, no_municipio, no_sigla_uf, nu_temperatura_minima, 
             nu_temperatura_maxima, no_variacao_temperatura, nu_umidade_minima, 
             nu_umidade_maxima, codigo_tempo, tempo, chuva, vento, no_direcao_vento, vento_variacao, 
             no_direcao_vento_variacao, periodo_chuva
                FROM portalmeteorologia.vw_previsao_tempo
                 WHERE co_municipio = '$id' AND dt_data_previsao >= current_date 
                 ORDER BY dt_data_previsao") );
        
        $entidade = new EntidadeRetorno();
        $entidade->dias = $results;
            return json_encode($entidade);
        } else {
            return json_encode(null);
        }
        
    }

    //
}

class EntidadeRetorno {
    public $dias;        
}
