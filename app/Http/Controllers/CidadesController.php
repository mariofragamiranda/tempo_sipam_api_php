<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class CidadesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll($key){

        if($key == "ha45664Hk214g5f66l89u11gf"){
            $results = DB::select( DB::raw("SELECT DISTINCT(co_municipio), no_municipio, no_sigla_uf FROM portalmeteorologia.vw_previsao_tempo") );
        
        $entidade = new EntidadeRetorno();
        $entidade->listaCidades = $results;

        return json_encode($entidade);
        } else {
            return json_encode(null);
        }

    }

    public function getAllUf($key, $uf){

        if($key == "ha45664Hk214g5f66l89u11gf"){
            $results = DB::select( DB::raw("SELECT DISTINCT(co_municipio), no_municipio, no_sigla_uf FROM portalmeteorologia.vw_previsao_tempo WHERE no_sigla_uf =  '$uf'") );
        
        $entidade = new EntidadeRetorno();
        $entidade->listaCidades = $results;
        
        return json_encode($entidade);
        } else {
            return json_encode(null);
        }

    }
  
}

class EntidadeRetorno {
    public $listaCidades;        
}
